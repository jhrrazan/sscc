// GetDataFromDB.js
import React, { useState, useEffect } from 'react';
import { View, Text } from 'react-native';
import connection from './db';

const GetDataFromDB = () => {
  const [data, setData] = useState([]);

  useEffect(() => {
    // Membuat kueri SQL
    const query = 'SELECT * FROM nama_tabel';

    // Mengirim kueri ke database
    connection.query(query, function (error, results, fields) {
      if (error) throw error;
      setData(results);
    });
  }, []);

  return (
    <View>
      <Text>Data dari Database:</Text>
      <Text>{JSON.stringify(data)}</Text>
    </View>
  );
};

export default GetDataFromDB;
