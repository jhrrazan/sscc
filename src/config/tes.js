import React, { useEffect } from "react";
import { View, Text } from "react-native";

const CallApi = () => {
  useEffect(() => {
    const formData = new FormData();
    formData.append("username", "916031");
    formData.append("password", "6031@T54t");

    fetch('https://3easy2.telkomsat.co.id/portal/api/auth', {
      method: 'POST',
      body: formData
    })
      .then(response => response.json())
      .then(data => {
        console.log(data);
      })
      .catch(error => {
        console.error(error);
      });
  }, []);

  return (
    <View>
      <Text>Ini adalah halaman CallApi</Text>
    </View>
  );
};

export default CallApi;
