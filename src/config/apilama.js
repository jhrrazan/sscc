import axios from 'axios';

const BASE_URL = 'http://192.168.100.7:3000/';

export const authenticateUser = async (username, password) => {
  try {
    const formData = new FormData();
    formData.append("username", username);
    formData.append("password", password);

    const response = await axios.post(`${BASE_URL}auth`, formData);

    // Axios akan menganggap response status 2xx sebagai sukses, jika tidak, akan masuk ke catch.
    if (response.status !== 200) {
      throw new Error('Authentication failed');
    }

    // Mengakses data respons menggunakan response.data
    const responseData = response.data;

    console.log(response.data);

    // Mengambil data nama dan NIK dari respons jika tersedia
    const { nama, nik } = responseData.data || {};

    // Mengembalikan objek dengan data nama dan NIK jika berhasil
    return { nama, nik };
  } catch (error) {
    throw error;
  }
};
