import axios from 'axios';

const BASE_URL = 'http://192.168.100.7:3000/api/';

export const authenticateUser = async (username, password) => {
  try {

    const response = await axios.post(`${BASE_URL}auth`, { username, password });

    // Axios akan menganggap response status 2xx sebagai sukses, jika tidak, akan masuk ke catch.
    if (response.status !== 200) {
      throw new Error('Authentication failed');
    }

    const responseData = response.data;

    // Mengenbalikan Data
    return responseData;
  } catch (error) {
    throw error;
  }
};
