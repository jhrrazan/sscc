import MySQL from 'react-native-mysql2';

const connection = MySQL.createConnection({
  host: 'alamat-host-database',
  user: 'nama-pengguna',
  password: 'kata-sandi',
  database: 'nama-database',
});

export default connection;