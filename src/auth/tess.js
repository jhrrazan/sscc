import React, { useState } from 'react';
import { View, TextInput, Button } from 'react-native';
import axios from 'axios';

const BackendAccessScreen = () => {
  const [username, setUsername] = useState('');
  const [password, setPassword] = useState('');

  const handleAccessBackend = async () => {
    try {
      const response = await axios.post('http://192.168.100.7:3000/auth', {
        username,
        password,
      });

      console.log('Respon dari backend:', response.data);
    } catch (error) {
      console.error('Terjadi kesalahan:', error);
    }
  };

  return (
    <View>
      <TextInput
        placeholder="Username"
        value={username}
        onChangeText={(text) => setUsername(text)}
      />
      <TextInput
        placeholder="Password"
        secureTextEntry
        value={password}
        onChangeText={(text) => setPassword(text)}
      />
      <Button title="Akses Backend" onPress={handleAccessBackend} />
    </View>
  );
};

export default BackendAccessScreen;
