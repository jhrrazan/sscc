import React, { useState, useEffect } from 'react';
import { View, Text, TextInput, Button, StyleSheet, Image, Alert, StatusBar } from 'react-native';
import { SafeAreaView } from 'react-native-safe-area-context';
import { authenticateUser } from "../config/api";
import { useRoute } from "@react-navigation/native";
import AsyncStorage from '@react-native-async-storage/async-storage';

const LoginScreen = ({ navigation }) => {

  const [username, setUsername] = useState("");
  const [password, setPassword] = useState("");

  const handleLogin = async () => {
    try {
      const data = await authenticateUser(username, password);

      if (data) {
        // Simpan data ke AsyncStorage
        await AsyncStorage.setItem("authData", JSON.stringify(data));
        await AsyncStorage.setItem("userId", data.nik); // Simpan userId di AsyncStorage

        // Tampilkan pesan berdasarkan role
        if (data.role === 'admin') {
          navigation.navigate('Admin');
        } else {
          navigation.navigate('User');
        }

        console.log(data);
      } else {
        Alert.alert("Login Gagal", "Autentifikasi Gagal. Periksa NIK dan Password Anda.");
      }
    } catch (error) {
      Alert.alert("Login Gagal", error.message);
    }
  };

  const handleLoginss = () => {

    if (username === '123' && password === '123') {
      navigation.navigate('Admin');
    } else {
      navigation.navigate('User');
    }
  };


  const route = useRoute();

  useEffect(() => {
    if (route.params?.clearCredentials) {
      setUsername("");
      setPassword("");
    }
  }, [route.params]);

  return (
    <SafeAreaView style={{ flex: 1, justifyContent: 'center', backgroundColor: '#f8f8ff' }}>
      <StatusBar barStyle="dark-content" backgroundColor="#ffffff" />
      <View style={styles.container}>
        <View style={styles.headerText}>
          <Text style={styles.textStyle}>Welcome To</Text>
          <Text style={styles.textStyle}>E - Book Troubleshooting</Text>
          <Text style={styles.textStyle}>PT Telkomsat</Text>
        </View>
        <View style={styles.imageContainer}>
          <Image
            source={require('../asset/images/LogoLogin.png')}
            style={styles.image}
          />
        </View>
        <View style={styles.inputContainer}>
          <TextInput
            style={styles.input}
            placeholder="Masukkan NIK"
            onChangeText={(text) => setUsername(text)}
            value={username}
            keyboardType="numeric"
          />
        </View>
        <View style={styles.inputContainer}>
          <TextInput
            style={styles.input}
            placeholder="Password"
            onChangeText={(text) => setPassword(text)}
            secureTextEntry
            value={password}
          />
        </View>
        <View style={styles.buttonContainer}>
          <Button title="Login" onPress={handleLogin} color="darkred" />
        </View>
        <Text style={{ textAlign: 'center', marginTop: 10, fontSize: 10, fontWeight: "bold" }}>
          Silahkan Login Menggunakan akun LDAP atau Email Telkomsat anda
        </Text>
      </View>
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    padding: 16,
  },
  imageContainer: {
    marginBottom: 40,
  },
  input: {
    flex: 1,
    height: 40,
  },
  inputContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    marginBottom: 17,
    width: '80%',
    borderColor: 'gainsboro',
    borderWidth: 1,
    padding: 10,
  },
  image: {
    width: 150,
    height: 150,
  },
  buttonContainer: {
    paddingTop: 35,
    width: '80%',
  },
  headerText: {
    marginTop: -50,
    marginBottom: 70,
  },
  textStyle: {
    fontWeight: 'bold',
    color: 'darkred',
    fontSize: 18,
  },
});

export default LoginScreen;
