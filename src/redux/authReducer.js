const initialState = {
    userRole: null, // Inisialisasi dengan null atau 'admin' / 'user' sesuai dengan kebutuhan
  };
  
  const authReducer = (state = initialState, action) => {
    switch (action.type) {
      case 'LOGIN_SUCCESS':
        return {
          ...state,
          userRole: action.payload, // Set peran pengguna setelah login berhasil
        };
      case 'LOGOUT':
        return {
          ...state,
          userRole: null, // Reset peran pengguna saat logout
        };
      default:
        return state;
    }
  };
  
  export default authReducer;