import { configureStore } from '@reduxjs/toolkit';
import authReducer from './authReducer'; // Pastikan mengacu pada reducer yang benar

const store = configureStore({
  reducer: {
    auth: authReducer, // Menggunakan nama slice 'auth'
  },
});

export default store;
