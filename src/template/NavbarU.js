import React from "react";
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import AkunScreenU from "../users/pages/akun";
import DownloadScreenU from "../users/pages/download";
import FavoriteScreenU from "../users/pages/favorite";
import HomeScreenUsers from "../users/pages/home"
import { View, Text, StyleSheet } from "react-native";
import Icon from 'react-native-vector-icons/Ionicons';

const Tab = createBottomTabNavigator();

const Tabs = () => {
    return (
        <Tab.Navigator
            screenOptions={{
                tabBarShowLabel: false,
                headerShown: false,
                tabBarStyle: {
                    position: "absolute",
                    bottom: 0,
                    right: 0,
                    left: 0,
                    height: 60,
                    elevation: 5,
                    backgroundColor: "#ffffff",
                }
            }} >

            <Tab.Screen name="Home" component={HomeScreenUsers} options={{
                tabBarIcon: ({ focused }) => (
                    <View style={styles.container}>
                        <Icon name="home-outline" size={25} color={focused ? "darkred" : "black"} />
                        <Text style={{ fontSize: 12, color: focused ? "darkred" : "black" }} >Home</Text>
                    </View>
                ),
            }} />
            <Tab.Screen name="Download" component={DownloadScreenU} options={{
                tabBarIcon: ({ focused }) => (
                    <View style={styles.container}>
                        <Icon name="cart-outline" size={25} color={focused ? "darkred" : "black"} />
                        <Text style={{ fontSize: 12, color: focused ? "darkred" : "black" }} >Download</Text>
                    </View>
                ),
            }} />
            <Tab.Screen name="Favorite" component={FavoriteScreenU} options={{
                tabBarIcon: ({ focused }) => (
                    <View style={styles.container}>
                        <Icon name="heart-outline" size={25} color={focused ? "darkred" : "black"} />
                        <Text style={{ fontSize: 12, color: focused ? "darkred" : "black" }} >Favorite</Text>
                    </View>
                ),
            }} />
            <Tab.Screen name="Akun" component={AkunScreenU} options={{
                tabBarIcon: ({ focused }) => (
                    <View style={styles.container}>
                        <Icon name="person-circle-outline" size={25} color={focused ? "darkred" : "black"} />
                        <Text style={{ fontSize: 12, color: focused ? "darkred" : "black" }}  >Profile</Text>
                    </View>
                ),
            }} />
        </Tab.Navigator>

    );
}

const styles = StyleSheet.create({
    container: {
        alignItems: 'center',
        justifyContent: 'center',
    },
});

export default Tabs;