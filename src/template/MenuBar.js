import React from 'react';
import { View, Text, TouchableOpacity } from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';
import { SafeAreaView } from 'react-native-safe-area-context';
import { useNavigation } from '@react-navigation/native';


const MenuBar = () => {

    const navigation = useNavigation();

    const homepress = () => {
        navigation.navigate('Home');
    }
    const profilePress = () => {
        navigation.navigate('Akun');
    }
    const downloadPress = () => {
        navigation.navigate('Download');
    }
    const FavoritePress = () => {
        navigation.navigate('Favorite');
    }

    return (
        <SafeAreaView style={{
            flexDirection: 'row',
            backgroundColor: '#ffffff',
            elevation: 3,
            paddingTop: 15,
            paddingBottom: 8,
        }}>
            <TouchableOpacity
                onPress={homepress}
                style={{
                    flex: 1,
                    justifyContent: 'center',
                    alignItems: 'center',
                }}>
                <Icon name="home-outline" size={25} color="black" />
                <Text style={{ fontSize: 12 }} >Home</Text>
            </TouchableOpacity>
            <TouchableOpacity
                onPress={downloadPress}
                style={{
                    flex: 1,
                    justifyContent: 'center',
                    alignItems: 'center',
                }} >
                <Icon name="cart-outline" size={25} color="black" />
                <Text style={{ fontSize: 12 }}>Download</Text>
            </TouchableOpacity>
            <TouchableOpacity
                onPress={FavoritePress}
                style={{
                    flex: 1,
                    justifyContent: 'center',
                    alignItems: 'center'
                }}>
                <Icon name="heart-outline" size={25} color="black" />
                <Text style={{ fontSize: 12 }}>Favorite</Text>
            </TouchableOpacity>
            <TouchableOpacity
                onPress={profilePress}
                style={{
                    flex: 1,
                    justifyContent: 'center',
                    alignItems: 'center'
                }}>
                <Icon name="person-circle-outline" size={25} color="black" />
                <Text style={{ fontSize: 12 }}>Profile</Text>
            </TouchableOpacity>
        </SafeAreaView>
    );
}

export default MenuBar;