import React from 'react';
import { createStackNavigator } from '@react-navigation/stack';
import { NavigationContainer } from '@react-navigation/native';
import ProfileScreen from "../admin/pages/profile";
import SirihSceen from "../admin/pages/sekapursirih";
import Daftar from "../admin/pages/daftar";
import TambahScreen from "../admin/pages/tambah";
import EditScreen from "../admin/pages/edit";
import Favorite from "../admin/pages/favorite";
import Download from "../admin/pages/download";
import TerakhirBaca from "../admin/pages/recent";
import PDF from "../admin/pages/pdfviewer_terakhir";
import PDF2 from "../admin/pages/pdfviewer_daftar";
import PDF3 from "../admin/pages/pdfviewer_favorite";
import Offline from "../admin/pages/offline";
import Navbar from "../template/Navbar";


const Stack = createStackNavigator();

const Navigation = () => {
    return (
        
            <Stack.Navigator screenOptions={{ headerShown: false }} initialRouteName="Navbar" >
                <Stack.Screen name='Navbar' component={Navbar} />
                <Stack.Screen name="PTelkomsat" component={ProfileScreen} />
                <Stack.Screen name="Sirih" component={SirihSceen} />
                <Stack.Screen name="Daftar" component={Daftar} />
                <Stack.Screen name="Tambah" component={TambahScreen} />
                <Stack.Screen name='EditP' component={EditScreen} />
                <Stack.Screen name='Favorite' component={Favorite} />
                <Stack.Screen name='Download' component={Download} />
                <Stack.Screen name='TerakhirBaca' component={TerakhirBaca} />
                <Stack.Screen name='PDFViewer_Terakhir' component={PDF} />
                <Stack.Screen name='PDFViewer_Daftar' component={PDF2} />
                <Stack.Screen name='PDFViewer_Favorite' component={PDF3} />
                <Stack.Screen name='Offline' component={Offline} />
            </Stack.Navigator>
        
    )
}

export default Navigation;