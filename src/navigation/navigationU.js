import React from 'react';
import { createStackNavigator } from '@react-navigation/stack';
import { NavigationContainer } from '@react-navigation/native';
import ProfileScreenU from "../users/pages/profile";
import SirihSceenU from "../users/pages/sekapursirih";
import DaftarU from "../users/pages/daftar";
import TambahScreenU from "../users/pages/tambah";
import EditScreenU from "../users/pages/edit";
import NavbarUsers from "../template/NavbarU";
import FavoriteU from "../users/pages/favorite";
import DownloadU from "../users/pages/download";
import TerakhirBacaU from "../users/pages/recent";
import PDFU from "../users/pages/pdfviewer_terakhir";
import PDF2U from "../users/pages/pdfviewer_daftar";
import PDF3U from "../users/pages/pdfviewer_favorite";
import OfflineU from "../users/pages/offline";


const Stack = createStackNavigator();

const Navigation = () => {
    return (
        
            <Stack.Navigator screenOptions={{ headerShown: false }} initialRouteName="NavbarU" >
                <Stack.Screen name='NavbarU' component={NavbarUsers} />
                <Stack.Screen name="PTelkomsatU" component={ProfileScreenU} />
                <Stack.Screen name="SirihU" component={SirihSceenU} />
                <Stack.Screen name="DaftarU" component={DaftarU} />
                <Stack.Screen name="TambahU" component={TambahScreenU} />
                <Stack.Screen name='EditPU' component={EditScreenU} />
                <Stack.Screen name='Favorite' component={FavoriteU} />
                <Stack.Screen name='Download' component={DownloadU} />
                <Stack.Screen name='TerakhirBaca' component={TerakhirBacaU} />
                <Stack.Screen name='PDFViewer_Terakhir' component={PDFU} />
                <Stack.Screen name='PDFViewer_Daftar' component={PDF2U} />
                <Stack.Screen name='PDFViewer_Favorite' component={PDF3U} />
                <Stack.Screen name='Offline' component={OfflineU} />
            </Stack.Navigator>
        
    )
}

export default Navigation;