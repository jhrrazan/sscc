import React, { useState } from "react";
import { NavigationContainer } from "@react-navigation/native";
import { createStackNavigator } from '@react-navigation/stack';
import SplashScreen from "../admin/pages/splash";
import LoginScreen from "../auth/login";
import NavigationA from "../navigation/navigationA";
import NavigationU from "../navigation/navigationU";


const Stack = createStackNavigator();

const MainNavigator = () => {
    return (
      <NavigationContainer>
        <Stack.Navigator initialRouteName="Splash" screenOptions={{ headerShown: false }}>
          <Stack.Screen name="Splash" component={SplashScreen} />
          <Stack.Screen name="Login" component={LoginScreen} />
          <Stack.Screen name="Admin" component={NavigationA} />
          <Stack.Screen name="User" component={NavigationU} />
        </Stack.Navigator>
      </NavigationContainer>
    );
  };
  
  export default MainNavigator;