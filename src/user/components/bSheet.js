import React, { forwardRef, useCallback, useImperativeHandle } from 'react';
import { StyleSheet, Text, View, TouchableOpacity, Dimensions } from 'react-native';
import Animated, { useSharedValue, useAnimatedStyle, withTiming } from 'react-native-reanimated';
import Icon from 'react-native-vector-icons/Ionicons';
import BackDrop from './BackDrop';

const BottomSheet = forwardRef((props, ref) => {
  const { snapTo } = props;
  const height = Dimensions.get('screen').height;
  const closeHeight = height;
  const percentage = parseFloat(snapTo.replace('%', '')) / 100;
  const openHeight = height * percentage;
  const topAnimation = useSharedValue(closeHeight);

  const expand = useCallback(() => {
    'worklet'
    topAnimation.value = withTiming(openHeight);
  }, [openHeight, topAnimation]);

  const close = useCallback(() => {
    'worklet'
    topAnimation.value = withTiming(closeHeight);
  }, [closeHeight, topAnimation]);

  useImperativeHandle(ref, () => ({
    expand,
    close,
  }), [expand, close]);

  const animationStyle = useAnimatedStyle(() => {
    const top = topAnimation.value;
    return {
      top,
    };
  });

  return (
    <>
      <BackDrop
        topAnimation={topAnimation}
        closeHeight={closeHeight}
        openHeight={openHeight}
        close={close}
      />
      <Animated.View style={[
        styles.container,
        animationStyle,
      ]}>
        <View style={styles.lineContainer}>
          <View style={styles.line} />
        </View>
        <View>
          <TouchableOpacity style={{ flexDirection: 'row', marginLeft: 15, marginTop: 17 }}>
            <Icon name="image-outline" size={28} color="black" />
            <Text style={styles.textContainer}>Pilih Dari Galeri</Text>
          </TouchableOpacity>
          <TouchableOpacity style={{ flexDirection: 'row', marginLeft: 15, marginTop: 17 }}>
            <Icon name="camera-outline" size={28} color="black" />
            <Text style={styles.textContainer}>Ambil Foto</Text>
          </TouchableOpacity>
        </View>
      </Animated.View>
    </>
  );
});

const styles = StyleSheet.create({
  container: {
    ...StyleSheet.absoluteFillObject,
    backgroundColor: '#ffffff',
    borderTopLeftRadius: 20,
    borderTopRightRadius: 20,
  },
  lineContainer: {
    alignItems: 'center',
    marginVertical: 15,
  },
  line: {
    width: 45,
    height: 4,
    backgroundColor: 'black',
    borderRadius: 10,
  },
  textContainer: {
    color: 'black',
    fontSize: 15,
    marginLeft: 10,
    marginTop: 3.5,
  },
});

export default BottomSheet;
