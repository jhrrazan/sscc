import React, { useState, useEffect } from 'react';
import { View, Text, TouchableOpacity, FlatList, StyleSheet } from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';
import AsyncStorage from '@react-native-async-storage/async-storage';
import axios from 'axios';
import { useNavigation } from '@react-navigation/native';

const App = () => {
  const navigation = useNavigation();
  const [clickedTitles, setClickedTitles] = useState([]);
  const [selectedEbook, setSelectedEbook] = useState(null);
  const [bukuList, setBukuList] = useState([]);
  const uniqueClickedTitles = Array.from(new Set(clickedTitles)); // Dapatkan judul unik
  const sortedUniqueClickedTitles = uniqueClickedTitles.sort(); // Sort judul untuk tampilan yang teratur
  const [clickedTitlesWithPage, setClickedTitlesWithPage] = useState([]);
  const [selectedSubjudul, setSelectedSubjudul] = useState(null);
  const [selectedTitle, setSelectedTitle] = useState(null); // Add new state variable

  useEffect(() => {
    axios.get('http://192.168.100.7:3001/api/getSubjudul')
      .then(response => {
        console.log('Data yang diterima:', response.data); // Tambahkan ini untuk melihat respons dari server
        setBukuList(response.data);
      })
      .catch(error => {
        console.log(error);
      });
  }, []);
  


    const bukuListWithSubjudul = bukuList && bukuList.map(buku => ({
    subjudul: buku.subjudul,
    id: buku._id,
    path: buku.path,
    name: buku.name
  }));

  // Menggunakan forEach untuk mencetak setiap objek dalam array
  bukuListWithSubjudul.forEach(data => {
    console.log('ID:', data.id);
    console.log('Subjudul:', data.subjudul);
    console.log('Path:', data.path);
    console.log('Name:', data.name);
    console.log('----------------------'); // Penanda untuk memisahkan setiap data
  });

  
  // const ebooks = bukuList && bukuList.map(buku => (
  //   { title: buku.title, name: buku.name, id: buku._id, path: buku.path }
  // ));

  const backPress = () => {
    navigation.navigate('Home');
  };

  const handleBuku = (clickedTitle) => {
    const selectedSubjudul = bukuList.find(buku => {
      if (typeof buku.subjudul === 'string' && typeof clickedTitle === 'string') {
        return buku.subjudul.includes(clickedTitle) || clickedTitle.includes(buku.subjudul);
      }
      return false;
    });
  
    if (selectedSubjudul) {
      const { subjudul, title, name, _id, path } = selectedSubjudul;
  
      const selectedTitle = selectedSubjudul.title || ''; // Pastikan selectedSubjudul memiliki properti title yang diinginkan
  
      console.log('Selected Subjudul:', selectedSubjudul);
      console.log('Selected Title:', selectedTitle);
  
      // Filter entri yang tidak sama dengan entri yang baru
      let updatedClickedTitles = clickedTitles.filter((clicked) => clicked !== clickedTitle);
  
      // Tambahkan entri baru ke array
      updatedClickedTitles = [clickedTitle, ...updatedClickedTitles.slice(0, 4)];
  
      setClickedTitles(updatedClickedTitles);
  
      AsyncStorage.setItem('clickedTitles', JSON.stringify(updatedClickedTitles))
        .then(() => {
          console.log('Clicked titles saved:', updatedClickedTitles);
        })
        .catch((error) => {
          console.error('Error saving clicked titles:', error);
        });
        
        console.log('Data yang dikirim saat navigasi:', {
          title: selectedTitle,
          subjudul,
          name,
          id: _id,
          path,
          clickedEntry: `${selectedTitle} - ${subjudul}`,
          clickedTitleEntry: `${selectedTitle} - ${subjudul}`,
          clickedTitle
        });
      // Navigasi ke layar PDFViewer dengan judul buku dan subjudul
      navigation.navigate('PDFViewer', { title: selectedTitle, subjudul, name, id: _id, path, clickedEntry: `${selectedTitle} - ${subjudul}`, clickedTitleEntry: `${selectedTitle} - ${subjudul}`, clickedTitle });
    }
  };
  

  // ambil judul buku terakhir baca
  useEffect(() => {
    const getLastViewedTitles = async () => {
      try {
        const value = await AsyncStorage.getItem('clickedTitles_${userId}');
        if (value !== null) {
          setClickedTitles(JSON.parse(value));
        }
      } catch (error) {
        console.error('Error retrieving clicked titles: ', error);
      }
    };

    getLastViewedTitles();
  }, []);


 // ambil page terakhir baca
  useEffect(() => {
    const getLastReadInfo = async () => {
      try {
        // Ambil informasi halaman terakhir setiap buku yang telah dibaca
        const lastReadInfoPromises = bukuList.map(async (buku) => {
          const lastPage = await AsyncStorage.getItem(`lastPage_${buku._id}_${userId}`);
          return { ...buku, lastPage: parseInt(lastPage) || 1 };
        });

        // Tunggu semua janji (promises) untuk mendapatkan informasi halaman terakhir
        const lastReadInfo = await Promise.all(lastReadInfoPromises);
        setClickedTitlesWithPage(lastReadInfo);

        console.log('Last read info:', lastReadInfo); // Tambahkan logging di sini
      } catch (error) {
        console.error('Error retrieving last read info:', error);
      }
    };

    getLastReadInfo();
  }, [bukuList, userId]);

  // kombinasi data judul dan halaman terakhir baca
  const combinedData = clickedTitles.map((title) => {
    const lastReadInfo = clickedTitlesWithPage.find((item) => {
      if (typeof item.subjudul === 'string' && typeof title === 'string') {
        if (item.subjudul.includes(title) || title.includes(item.subjudul)) {
          console.log('Kalimat yang sama ditemukan:', item.subjudul);
          return true; // Jika kalimat sama, kembalikan true
        }
      }
      return false;
    });
  
    // Periksa apakah nilai lastReadInfo dan lastPage tersedia
    console.log('Last Read Info:', lastReadInfo);
    console.log('Last Page:', lastReadInfo ? lastReadInfo.lastPage : 'No Last Page Info');
  
    return { ...lastReadInfo, title };
  });
  
  



  // useEffect(() => {
  //   const getLastReadInfo = async () => {
  //     try {
  //       // Ambil informasi halaman terakhir setiap subjudul dari setiap buku yang telah dibaca
  //       const lastReadInfoPromises = bukuList.map(async (buku) => {
  //         const lastSubjudulInfoPromises = buku.subjudul.map(async (sub) => {
  //           const lastPage = await AsyncStorage.getItem(lastPage_${sub._id});
  //           console.log(Subjudul ID: ${sub._id}, Last Page: ${lastPage});
  //           return { ...sub, lastPage: parseInt(lastPage) || 1 };
  //         });
  //         const lastSubjudulInfo = await Promise.all(lastSubjudulInfoPromises);
  //         return { ...buku, subjudul: lastSubjudulInfo };
  //       });
  
  //       // Tunggu semua janji (promises) untuk mendapatkan informasi halaman terakhir dari subjudul
  //       const lastReadInfo = await Promise.all(lastReadInfoPromises);
  //       setClickedTitlesWithPage(lastReadInfo);
  //     } catch (error) {
  //       console.error('Error retrieving last read info:', error);
  //     }
  //   };
  
  //   getLastReadInfo();
  // }, [bukuList]);
  
  


  return (
    <View style={styles.container}>
      <View style={styles.header}>
        <TouchableOpacity style={{ padding: 10 }} onPress={backPress}>
          <Icon name="arrow-back" size={25} color="white" />
        </TouchableOpacity>
        <Text style={styles.headerText}>Terakhir Baca</Text>
      </View>
  
      <View style={styles.bookItemContainer}>
        <FlatList
          data={combinedData}
          keyExtractor={(item, index) => index.toString()}
          renderItem={({ item }) => (
            <TouchableOpacity onPress={() => handleBuku(item.title)}>
              <View style={styles.itemContainer}>
                <Text style={styles.item}>{item.title}</Text>
                <Text style={{ fontSize: 16, color: 'gray' }}>
                  Halaman Terakhir Baca : {item.lastPage}
                </Text>
              </View>
            </TouchableOpacity>
          )}
          ItemSeparatorComponent={() => <View style={styles.separator} />}
        />
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white',
  },
  headerText: {
    fontSize: 25,
    color: 'white',
    fontWeight: 'bold',
    marginLeft: 20,
  },
  item: {
    fontSize: 20,
    fontWeight: 'bold',
    color: 'black',
    marginBottom: 10,
  },
  header: {
    flexDirection: 'row',
    alignItems: 'center',
    padding: 15,
    backgroundColor: 'maroon',
    borderBottomWidth: 1,
    borderBottomColor: 'maroon',
  },
  bookItemContainer: {
  flexDirection: 'row',
  marginHorizontal: 20,
  alignItems: 'center',
  paddingVertical: 20,
  },
  separator: {
    marginBottom: 10,
    height: 1,
    backgroundColor: 'gray', 
  },
  itemContainer: {
    marginBottom: 20,
  },
});

export default App;