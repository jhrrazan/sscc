import React from 'react';
import { View, Text, StyleSheet, TouchableOpacity } from 'react-native';
import Pdf from 'react-native-pdf';
import RNFS from 'react-native-fs';
import Icon from 'react-native-vector-icons/Ionicons';
import { useNavigation } from '@react-navigation/native';

const App = ({ route }) => {
  const navigation = useNavigation();
  const { book, path, text, title, subjudul } = route.params; // Receiving book information and path from navigation

  console.log('Received title:', title);

  const backPress = () => {
    navigation.navigate('Download');
  };

  const source = { uri: `file://${path}`, cache: true };

  return (
    <View style={styles.container}>
      <View style={styles.header}>
        <TouchableOpacity style={styles.backButton} onPress={backPress}>
          <Icon name="arrow-back" size={25} color="white" />
        </TouchableOpacity>
        <Text style={styles.headerText}>{title}</Text>
      </View>
      <Pdf
        source={source}
        onLoadComplete={(numberOfPages, filePath) => {
          console.log(`Number of pages: ${numberOfPages}`);
        }}
        onError={(error) => {
          console.error('Error displaying PDF:', error);
        }}
        style={{ flex: 1, width: '100%', height: '100%' }}
      />
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white',
  },
  header: {
    flexDirection: 'row',
    alignItems: 'center',
    paddingVertical: 15,
    paddingHorizontal: 20,
    backgroundColor: 'red',
    borderBottomWidth: 1,
    borderBottomColor: 'red',
  },
  backButton: {
    padding: 10,
  },
  headerText: {
    fontSize: 25,
    color: 'white',
    fontWeight: 'bold',
    marginLeft: 20,
  },
});

export default App;
