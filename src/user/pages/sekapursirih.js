import React, { useState, useEffect } from 'react';
import {View, Text, TouchableOpacity, StatusBar, Image, StyleSheet, ScrollView} from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';
import { useNavigation } from '@react-navigation/native';
import { SafeAreaView } from 'react-native-safe-area-context';

const App = () => {

    const navigation = useNavigation();

    const backPress = () => {
        navigation.navigate('HomeA');
    }

    return (
        <SafeAreaView style={{flex:1, backgroundColor: '#f8f8ff' }}>
            <View style={{
                marginHorizontal: 20,  
                flexDirection: 'row', 
                marginTop: 10 ,
                }}>
                <StatusBar barStyle={'dark-content'} backgroundColor={'#f8f8ff'}/>
                <TouchableOpacity style={{marginTop: 5}} onPress={backPress}>
                    <Icon name="arrow-back" size={25} color="black" />
                </TouchableOpacity>
                <Text style={{fontSize: 25, color: 'darkred', paddingLeft: 20 }}>Sekapur Sirih</Text>
            </View>
            <ScrollView>
                <View style={{flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center', marginTop: 10 }}>
                    <Image source={ require ('../../asset/images/Picture2.jpg')} style={{height:50, width: 180, marginLeft: 3}} />
                    <Image source={ require ('../../asset/images/Picture1.png')} style={{height:50, width:50, marginRight: 5}} />
                </View>
                <View style={{alignItems:'center'}}>
                    <Text style={styles.header}>Sekapur Sirih</Text>
                </View>
                <View style={styles.containerHeader}>
                    <Text style={styles.textP}>Di era keterbukaan ini kita dihadapkan pada situasi dimana informasi telah menjadi kebutuhan dan bukan lagi sekedar keharusan. 
                    Terlebih lagi untuk melakukan sebuah keputusan maka informasi itu tidak cukup hanya sekedar ada melainkan harus didukung pula dengan data yang akurat 
                    sekaligus mudah dan cepat untuk diakses. Maka, satu-satunya jalan informasi tersebut harus diantarkan melalui seluran transmisi yang handal dan dalam hal 
                    ini TELKOMSAT menjadi salah satu perusahaan yang dipercaya dalam melakukan proses tersebut </Text>
                    <Text style={styles.textP2 }>Selama ini kita telah memberikan services kepada berbagai macam pelanggan dengan karakter yang berbeda-beda, seperti : Goverment, 
                    Media Cetak, Broadcasting, Oil  & Gas Company, Mining & Plantation, Perbankan,  Telekomunikasi, Maritim dan lain-lain untuk memenuhi kebutuhan akan informasi 
                    baik dalam bentuk voice, data ataupun aplikasi. Kehandalan Services dan kecepatan Respons menjadi kunci sukses dalam mempertahankan Loyalitas Pelanggan.</Text>
                    <Text style={styles.textP2 }>Oleh karena itu dengan dibuatnya Buku Panduan Operasional sebagai pegangan setiap Engineer atau Technician di seluruh jajaran 
                    subdit Network Operation dalam upaya meningkatkan Kualitas Layanan kepada Pelanggan secara berkesinambungan sehingga akan menjadi lebih baik (Excellent). 
                    Kami menyadari buku panduan ini belum sempurna, partisipasi dan peran aktif seluruh jajaran subdit Network Operation dalam memberikan ide, masukan ataupun 
                    kritik dan saran sangat diharapkan untuk perbaikan di masa depan. </Text>
                    <Text style={styles.textP2 }>Terimakasih atas kerjasamanya kepada pihak-pihak yang telah membantu dalam pembuatan buku ini, semoga Alloh SWT meridhoi 
                    setiap aktifitas yang kita lakukan demi mewujudkan tujuan perusahaan untuk kesejahteraan seluruh Stake Holder. </Text>
                </View>
                <View style={{justifyContent: 'space-between', alignItems: 'flex-end', marginHorizontal: 20 }}>
                        <Text style={styles.textP2}>Bogor, September 2023</Text>
                        <Text style={styles.textP2}>Managemen</Text>
                        <Text style={styles.textP2}>GM.Assurance</Text>
                        <Text style={styles.textP3}>Teguh Priyono, ST. MM</Text>
                </View>

            </ScrollView>
        </SafeAreaView>
    );
}

const styles = StyleSheet.create({
    containerHeader: {
      flex: 1,
      marginHorizontal: 20,
      marginVertical: 10,
      marginTop: 25,
      alignItems: 'flex-start', 
    },
    header: {
      fontSize: 20,
      color: 'black',
      marginTop: 20 ,
    },
    textP: {
      fontSize: 16,
      color: 'black',
    },
    textP2: {
      fontSize: 16,
      color: 'black',
      marginTop: 8
    },
    textP3: {
      fontSize: 16,
      color: 'black',
      marginTop: 50
    },
});

export default App;