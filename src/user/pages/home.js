import React from 'react';
import { View, Text, Image, Dimensions, StatusBar, StyleSheet, TouchableOpacity, ScrollView} from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';
import { SafeAreaView } from 'react-native-safe-area-context';
import MenuBar from '../../template/MenuBar';
import { useNavigation } from '@react-navigation/native';

const { width } = Dimensions.get('window');

const App = () => {

  const navigation = useNavigation();

  const profilePress = () => {
    navigation.navigate('PTelkomsat');
  }
  const sirihPress = () => {
    navigation.navigate('Sirih');
  }
  const daftarPress = () => {
    navigation.navigate('Daftar');
  }

  return (
    <SafeAreaView style={{ flex: 1, backgroundColor: '#f8f8ff' }}>
      <View style={{ flex: 1 }}>
        <View style={styles.header}>
          <StatusBar barStyle={'dark-content'} backgroundColor={'#f8f8ff'} />
          <TouchableOpacity style={styles.menuIcon}>
            <Icon name="menu" size={25} color="black" />
          </TouchableOpacity>
          <Text style={styles.headerText}>Home</Text>
        </View>
        <ScrollView>
          <View style={styles.imageContainer}>
            <View style={styles.imageWrapper}>
              <Image
                source={require('../../asset/images/home.jpg')}
                style={styles.homeImage}
              />
            </View>
          </View>
          <View style={styles.gridContainer}>
            <TouchableOpacity
              style={styles.itemContainer}
              onPress={profilePress}
            >
              <Image
                source={require('../../asset/images/profile.png')}
                style={styles.itemImage}
              />
              <Text style={styles.itemText}>Profile Telkomsat</Text>
            </TouchableOpacity>
            <TouchableOpacity
              style={styles.itemContainer}
              onPress={sirihPress}
            >
              <Image
                source={require('../../asset/images/sekapursirih.png')}
                style={styles.itemImage}
              />
              <Text style={styles.itemText}>Sekapur Siri</Text>
            </TouchableOpacity>
            <TouchableOpacity
              style={styles.itemContainer}
              onPress={daftarPress}
            >
              <Image
                source={require('../../asset/images/daftar.png')}
                style={styles.itemImage}
              />
              <Text style={styles.itemText}>Daftar Buku</Text>
            </TouchableOpacity>
            <TouchableOpacity
              style={styles.itemContainer}
              //onPress={() => navigation.navigate('TerakhirBacaScreen')}
            >
              <Image
                source={require('../../asset/images/baca.png')}
                style={styles.itemImage}
              />
              <Text style={styles.itemText}>Terakhir Baca</Text>
            </TouchableOpacity>
          </View>
        </ScrollView>
      </View>
      <MenuBar />
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  header: {
    marginHorizontal: 20,
    flexDirection: 'row',
    marginTop: 10,
  },
  menuIcon: {
    marginTop: 5,
  },
  headerText: {
    fontSize: 25,
    color: 'darkred',
    paddingLeft: 20,
  },
  imageContainer: {
    alignItems: 'center',
    marginTop: 35,
  },
  imageWrapper: {
    width: width * 0.9,
    height: width * 0.5,
    backgroundColor: 'lightgray',
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 20,
  },
  homeImage: {
    width: '100%',
    height: '100%',
    resizeMode: 'cover',
    borderRadius: 20,
  },
  gridContainer: {
    flexDirection: 'row',
    flexWrap: 'wrap',
    justifyContent: 'space-between',
    marginHorizontal: 25,
    marginTop: 45,
  },
  itemContainer: {
    backgroundColor: '#ffffff',
    width: '48%', // Menggunakan 48% dari lebar parent untuk jarak antar elemen
    marginBottom: 15,
    borderRadius: 20,
    alignItems: 'center',
    justifyContent: 'center',
    paddingVertical: 25,
  },
  itemText: {
    marginTop: 5,
    fontSize: 12,
    fontWeight: 'bold',
  },
  itemImage: {
    width: 50,
    height: 80,
    resizeMode: 'contain',
  },
});

export default App;
