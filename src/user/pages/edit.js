import React, { useState, useEffect, useRef, useCallback } from 'react';
import {View, Text, TouchableOpacity, StatusBar, StyleSheet, Image, SafeAreaView, TouchableWithoutFeedback} from 'react-native';
import { useNavigation } from '@react-navigation/native';
import AsyncStorage from '@react-native-async-storage/async-storage';
import {GestureHandlerRootView} from 'react-native-gesture-handler';
import {SafeAreaProvider} from 'react-native-safe-area-context';
import BottomSheet, { BottomSheetMethods } from '../components/bSheet';

const Edit = () => {

    const [nama, setNama] = useState("");
    const [nik, setNik] = useState("");
    
    const navigation = useNavigation();

    const batalHandler = () => {
        navigation.navigate('Akun');
    }
    
    const bottomSheetRef = useRef(null); // Buat ref menggunakan useRef

    const expandHandler = useCallback(() => {
        bottomSheetRef.current.expand(); // Menggunakan ref yang telah dibuat
    }, []);

    useEffect(() => {
        async function fetchData() {
          try {
            const authData = await AsyncStorage.getItem('authData');
            if (authData) {
              const parsedAuthData = JSON.parse(authData);
    
              if (parsedAuthData.nama && parsedAuthData.nik) {
                setNama(parsedAuthData.nama);
                setNik(parsedAuthData.nik);
              }
            }
          } catch (error) {
            console.error('Error fetching data:', error);
          }
        }
    
        fetchData();
    }, []); 
      
    return (
        <SafeAreaProvider>
            <GestureHandlerRootView style={{ flex: 1 }}>
                <SafeAreaView style={{flex:1, backgroundColor: '#f8f8ff'}}>
                    <StatusBar barStyle={'dark-content'} backgroundColor={'darkred'}/>
                    <View style={{backgroundColor: 'darkred', paddingBottom:15}}>
                        <View style={{
                            flexDirection: 'row',
                            justifyContent: 'space-between',
                            marginTop: 10,
                            }}>
                            <TouchableOpacity onPress={batalHandler} >
                                <Text style={{ fontSize: 18, color: '#ffffff', marginLeft: 10}}>Batal</Text>
                            </TouchableOpacity>
                            <Text style={{ fontSize: 20, color: '#ffffff', fontWeight: 'bold'}}>Edit Profile</Text>
                            <TouchableOpacity>
                                <Text style={{ fontSize: 18, color: '#ffffff', marginRight: 10}}>Selesai</Text>
                            </TouchableOpacity>
                        </View>
                    </View>
                        <View style={{flex:1, alignItems:'center', marginTop: 20}}>
                                <View style={styles.imageBackground}>
                                    {/* ISI GAMBAR DISINI  */}
                                </View>
                                <TouchableOpacity onPress={() => expandHandler()} style={{marginTop:15}}>
                                    <Text style={{color:'black', fontSize: 15, fontWeight: 'bold'}}>Edit Foto</Text>
                                </TouchableOpacity>
                                <View style={{marginTop: 50, alignItems:'center'}}>
                                    <Text style={{color: 'black', fontWeight: 'bold', fontSize: 18}}>Hi</Text>
                                    <Text style={{ color: 'black', fontWeight: 'bold', fontSize: 18 }}>{nama}</Text>
                                    <Text style={{ color: 'black' }}>{nik}</Text>
                                </View>
                        </View>
                    <BottomSheet ref={bottomSheetRef} snapTo={'75%'} />
                </SafeAreaView>
            </GestureHandlerRootView>
        </SafeAreaProvider>
    );
}

const styles = StyleSheet.create({
    imageBackground: {
        width: 125,
        height: 125,
        marginTop: 30,
        borderRadius: 70,
        borderColor:'black',
        borderWidth: 1,
    },
});

export default Edit;