import React, { useState, useEffect } from 'react';
import { View, Text, TouchableOpacity, StatusBar, Image, Dimensions, FlatList, StyleSheet } from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';
import MenuBar from '../../template/MenuBar';

const { width } = Dimensions.get('window');

const data = [
    { id: '1', text: 'Ebook 1', imageSource: require('../../asset/images/sample.jpg') },
    { id: '2', text: 'Ebook 2', imageSource: require('../../asset/images/sample.jpg') },
];

const Separator = () => <View style={{ height: 20 }} />; // Adjust the height based on your preference

const BookItem = ({ item, onDeletePress }) => (
    <View style={{ flexDirection: 'row', marginHorizontal: 20, alignItems: 'center' }}>
        <Image
            source={item.imageSource}
            style={{ width: width * 0.3, height: width * 0.5, resizeMode: 'cover', borderRadius: 20, overflow: 'hidden' }}
        />
        <View style={{ marginLeft: 20, flex: 1 }}>
            <Text style={{ fontSize: 20, color: 'black' }}>{item.text}</Text>
            {/* Additional information or subtitle if needed */}
            <Text style={{ fontSize: 16, color: 'gray' }}>Author Name</Text>
        </View>
        <TouchableOpacity onPress={() => onDeletePress(item.id)} style={{ position: 'absolute', bottom: 5, right: 5 }}>
            <Icon name="trash-bin-outline" size={20} color="red" />
        </TouchableOpacity>
    </View>
);

const App = () => {
    const handleDelete = (itemId) => {
        // Implement your logic to delete the item from the data array
        // For example: setData(data.filter(item => item.id !== itemId));
    };

    return (
        <View style={{ flex: 1, backgroundColor: '#ffffff' }}>
            <View style={{
                marginHorizontal: 20,
                flexDirection: 'row',
                alignItems: 'center',
                margin: 10,
            }}>
                <StatusBar barStyle={'dark-content'} backgroundColor={'#f8f8ff'} />
                <TouchableOpacity style={{ marginTop: 5 }}>
                    <Icon name="menu" size={25} color="black" />
                </TouchableOpacity>
                <Text style={{ fontSize: 25, color: 'crimson', paddingLeft: 20 }}>Download</Text>
            </View>

            <FlatList
                data={data}
                keyExtractor={(item) => item.id}
                renderItem={({ item }) => <BookItem item={item} onDeletePress={handleDelete} />}
                ItemSeparatorComponent={() => <Separator />}
            />

            <MenuBar />
        </View>
    );
}

const styles = StyleSheet.create({

});

export default App;
