import React, { useState, useEffect } from 'react';
import {View, Text, TouchableOpacity, StatusBar, StyleSheet, Image} from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';
import { SafeAreaView } from 'react-native-safe-area-context';
import { useNavigation } from '@react-navigation/native';
import MenuBar from '../../template/MenuBar';
import AsyncStorage from '@react-native-async-storage/async-storage';

const Akun = () => {

    const [nama, setNama] = useState("");
    const [nik, setNik] = useState("");
    const navigation = useNavigation();

    const backPress = () => {
        navigation.navigate('HomeA');
      }

    const editHandler = () => {
        navigation.navigate('EditP');
    }

      useEffect(() => {
        async function fetchData() {
          try {
            // Ambil data autentikasi dari AsyncStorage
            const authData = await AsyncStorage.getItem('authData');
            if (authData) {
              const parsedAuthData = JSON.parse(authData);
    
              // Pastikan bahwa struktur data sesuai
              if (parsedAuthData.nama && parsedAuthData.nik) {
                setNama(parsedAuthData.nama);
                setNik(parsedAuthData.nik);
              }
            }
          } catch (error) {
            console.error('Error fetching data:', error);
          }
        }
    
        fetchData();
      }, []); 
      
    const handleLogout = async () => {
        try {
          // Hapus data otentikasi dari asyncronous storage saat logout
          await AsyncStorage.removeItem('authData');
          console.log('Data otentikasi telah dihapus.');

          navigation.navigate('Login', { clearCredentials: true });

        } catch (error) {
          console.error('Error during logout:', error);
        }
    };

    return (
        <SafeAreaView style={{flex:1, backgroundColor: '#f8f8ff'}}>
            <View style={{backgroundColor: 'darkred', paddingBottom:60, borderBottomRightRadius:20, borderBottomLeftRadius:20}}>
                <StatusBar barStyle={'dark-content'} backgroundColor={'darkred'}/>
                <View style={{
                    marginHorizontal: 20,
                    flexDirection: 'row',
                    marginTop: 10,
                    alignItems: 'center',
                    }}>
                    <TouchableOpacity style={{ marginTop: 5 }} onPress={backPress}>
                        <Icon name="arrow-back" size={25} color="black" />
                    </TouchableOpacity>
                    <Text style={{ fontSize: 25, color: '#ffffff', paddingLeft: 20 }}>Profile</Text>
                </View>
                <View style={{flexDirection: 'row'}}>
                    <View style={styles.imageBackground}>
                        {/* ISI GAMBAR DISINI  */}
                    </View>
                    <View style={{marginTop: 50,}}>
                        <Text style={{color: '#ffffff', fontWeight: 'bold', fontSize: 18}}>Hi</Text>
                        <Text style={{ color: '#ffffff', fontWeight: 'bold', fontSize: 18 }}>{nama}</Text>
                        <Text style={{ color: '#ffffff' }}>{nik}</Text>
                    </View>
                </View>
            </View>
            <View style={styles.Container}>
                <View style={styles.gridContainer}>
                    <TouchableOpacity onPress={editHandler} style={styles.itemContainer}>
                        <Icon name="create-outline" size={25} color="black" style={{marginLeft:10}} />
                        <View>
                            <Text style={styles.itemText}>Edit Profile</Text>
                        </View>
                    </TouchableOpacity>
                    <TouchableOpacity style={styles.itemContainer}>
                        <Icon name="help-circle-outline" size={25} color="black" style={{marginLeft:10}} />
                        <View>
                            <Text style={styles.itemText}>Bantuan</Text>
                        </View>
                    </TouchableOpacity>
                    <TouchableOpacity onPress={handleLogout} style={styles.itemContainer}>
                        <Icon name="log-out-outline" size={25} color="black" style={{marginLeft:10}} />
                        <Text style={styles.itemText}>Logout</Text>
                    </TouchableOpacity>
                </View>
            </View>
            
            <MenuBar/>
        </SafeAreaView>
    );
}

const styles = StyleSheet.create({
    imageBackground: {
        width: 125,
        height: 125,
        backgroundColor: 'grey',
        marginHorizontal: 20,
        marginTop: 30,
        borderRadius: 30,
    },
    Container: {
        flex: 1,
        marginHorizontal: 25,
        marginTop: 30,
    },
    gridContainer: {
        marginTop: 10,
        justifyContent: 'space-between',
        
    },
    itemContainer: {
        backgroundColor: '#ffffff',
        borderRadius: 10,
        flexDirection: 'row',
        paddingBottom: 15,
        paddingTop: 15,
        marginBottom: 25,
        elevation: 5,

    },
    itemText: {
        marginLeft: 15,
        fontSize: 15,
        color: 'black',
    },
    imageItem: {
        marginLeft: 10,
        width: 30,
        height: 30,
        resizeMode: 'contain',
      },
});

export default Akun;