import React, { useState } from 'react';
import { View, Text, TouchableOpacity, StatusBar, Image, Dimensions, FlatList, StyleSheet } from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';
import MenuBar from '../../template/MenuBar';

const { width } = Dimensions.get('window');

const data = [
    { id: '1', text: 'Ebook 1', imageSource: require('../../asset/images/sample.jpg') },
    { id: '2', text: 'Ebook 2', imageSource: require('../../asset/images/sample.jpg') },
];

const Separator = () => <View style={{ height: 20 }} />; // Adjust the height based on your preference

const BookItem = ({ item }) => {
    const [isLoved, setLoved] = useState(false);

    const toggleLove = () => {
        setLoved(!isLoved);
    };

    return (
        <View style={{ flexDirection: 'row', marginHorizontal: 20, alignItems: 'center' }}>
            <Image
                source={item.imageSource}
                style={{ width: width * 0.3, height: width * 0.5, resizeMode: 'cover', borderRadius: 20, overflow: 'hidden' }}
            />
            <View style={{ marginLeft: 20, flex: 1 }}>
                <Text style={{ fontSize: 20, color: 'black' }}>{item.text}</Text>
                {/* Additional information or subtitle if needed */}
                <Text style={{ fontSize: 16, color: 'gray' }}>Author Name</Text>
            </View>
            <TouchableOpacity onPress={toggleLove} style={{ position: 'absolute', bottom: 5, right: 5 }}>
                <Icon name={isLoved ? 'heart' : 'heart-outline'} size={20} color={isLoved ? 'red' : 'black'} />
            </TouchableOpacity>
        </View>
    );
};

const App = () => {
    return (
        <View style={{ flex: 1, backgroundColor: '#ffffff' }}>
            <View style={{
                marginHorizontal: 20,
                flexDirection: 'row',
                alignItems: 'center',
                margin: 10,
            }}>
                <StatusBar barStyle={'dark-content'} backgroundColor={'#f8f8ff'} />
                <TouchableOpacity style={{ marginTop: 5 }}>
                    <Icon name="menu" size={25} color="black" />
                </TouchableOpacity>
                <Text style={{ fontSize: 25, color: 'crimson', paddingLeft: 20 }}>Favorite</Text>
            </View>

            <FlatList
                data={data}
                keyExtractor={(item) => item.id}
                renderItem={({ item }) => <BookItem item={item} />}
                ItemSeparatorComponent={() => <Separator />}
            />

            <MenuBar />
        </View>
    );
}

const styles = StyleSheet.create({

});

export default App;
