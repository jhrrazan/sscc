  import React, { useState, useEffect } from 'react';
  import {
    StyleSheet,
    Dimensions,
    View,
    StatusBar,
    TouchableOpacity,
    ActivityIndicator,
    Alert,
    Linking,
    Text,
  } from 'react-native';
  import Pdf from 'react-native-pdf';
  import MenuBar from '../templates/MenuBar';
  import { useNavigation, useRoute } from '@react-navigation/native'; // Menghapus useState dari sini
  import Icon from 'react-native-vector-icons/Ionicons';
  import Icon2 from 'react-native-vector-icons/Fontisto';
  import AsyncStorage from '@react-native-async-storage/async-storage'; // Import AsyncStorage
  import RNFetchBlob from 'rn-fetch-blob';

  const App = () => {
    const navigation = useNavigation();
    const route = useRoute();
    const [favoriteTitles, setFavoriteTitles] = useState([]); // Tetap gunakan useState dari 'react'
    const [currentPage, setCurrentPage] = useState(1); // Simpan halaman saat ini
    const [lastViewedPage, setLastViewedPage] = useState(1);

    const onPageChanged = (page, numberOfPages) => {
      // Ketika halaman berubah, simpan halaman terakhir
      setCurrentPage(page);
    }

    const handleBuku = () => {
      navigation.goBack();
    };

    const { name, id : _id , title, subjudul, clickedEntry, clickedTitle } = route.params;

    const handleSearch = () => {
      navigation.navigate('Search'); // Navigasi ke layar pencarian
    };

    const source = { uri: `http://192.168.100.7:3001/api/pdfSubjudul/${_id}/${name}`, cache: true };
    // const source = { uri: `http://192.168.100.7:3001/api/pdf/${id}/${name}`, cache: true };
    // const source = { uri: `http://192.168.100.3:3001/api/pdf/6543ada1468ad59e03a13708/topologi.pdf`, cache: true };

    const handleDownload = async () => {
      const downloadURL = source.uri;
      const destinationPath = `${RNFetchBlob.fs.dirs.DownloadDir}/${name}.pdf`;
      const downloadedBook = {
        id: Math.random().toString(36).substring(7), // Tambahkan properti id unik
        _id,
        text: clickedTitle,
        fileName: name,
        url: destinationPath,
      };
    
      try {
        const storedBooks = await AsyncStorage.getItem('downloadedBooks');
        const existingBooks = storedBooks ? JSON.parse(storedBooks) : [];
    
        const existingBookIndex = existingBooks.findIndex((book) => book.text === clickedTitle);
    
        if (existingBookIndex !== -1) {
          const existingBook = existingBooks[existingBookIndex];
          if (existingBook.fileName !== name) {
            existingBooks[existingBookIndex] = downloadedBook;
            await AsyncStorage.setItem('downloadedBooks', JSON.stringify(existingBooks));
            Alert.alert('Replace Successful', `${existingBook.text} replaced in downloaded books!`);
          } else {
            Alert.alert('Book Already Exists', `A book with the title "${existingBook.text}" already exists.`);
          }
        } else {
          existingBooks.push(downloadedBook);
          await AsyncStorage.setItem('downloadedBooks', JSON.stringify(existingBooks));
    
          RNFetchBlob.config({
            addAndroidDownloads: {
              useDownloadManager: true,
              notification: true,
              path: destinationPath,
              description: 'PDF File',
            },
            fileCache: true,
            appendExt: 'pdf',
          })
          .fetch('GET', downloadURL)
          .then(() => {
            Alert.alert('Download Successful', `File downloaded successfully to ${destinationPath}`);
          })
          .catch((error) => {
            Alert.alert('Download Failed', `File download failed: ${error}`);
          });
          Alert.alert('Berhasil Download');
        }
      } catch (error) {
        console.error('Error handling download:', error);
      }
    };
    
    
    
    

    // Fungsi untuk menambah judul ke daftar favorit dan menyimpannya di AsyncStorage
    const addToFavorites = async (clickedTitle) => {
      try {
        setFavoriteTitles(prevTitles => [...prevTitles, clickedTitle]);
        await AsyncStorage.setItem('favoriteTitles', JSON.stringify([...favoriteTitles, clickedTitle]));
        Alert.alert('Success', `${clickedTitle} added to favorites!`);
      } catch (error) {
        Alert.alert('Error', 'Failed to add to favorites');
      }
    };

    // favorit
    useEffect(() => {
      // Ambil daftar judul favorit dari AsyncStorage saat komponen dimuat
      const fetchData = async () => {
        try {
          const storedTitles = await AsyncStorage.getItem('favoriteTitles');
          if (storedTitles) {
            setFavoriteTitles(JSON.parse(storedTitles));
          }
        } catch (error) {
          // Handle error if needed
          console.error(error);
        }
      };

      fetchData();
    }, []);

    // last page
    useEffect(() => {
    console.log(`ID yang masuk: ${_id}`);
    return () => {
      // Simpan halaman terakhir saat pengguna meninggalkan halaman
      AsyncStorage.setItem(`lastPage_${_id}_${userId}`, currentPage.toString())
        .then(() => {
          console.log(`Last page ${currentPage} saved for ${subjudul}`);
        })
        .catch((error) => {
          console.error('Error saving last page:', error);
        });
    };
    }, [currentPage, _id, subjudul, userId]);

    // page terakhir dibaca
    useEffect(() => {
      const fetchData = async () => {
        try {
          const storedPage = await AsyncStorage.getItem(`lastPage_${_id}_${userId}`);
          if (storedPage) {
            setCurrentPage(parseInt(storedPage));
          }
        } catch (error) {
          console.error('Error fetching last page:', error);
        }
      };
    
      fetchData();
    }, [userId]);
    
    // page terakhir dibaca
    useEffect(() => {
      const fetchData = async () => {
        try {
          const storedPage = await AsyncStorage.getItem(`lastPage_${_id}_${userId}`);
          if (storedPage) {
            setLastViewedPage(parseInt(storedPage));
          }
        } catch (error) {
          console.error('Error fetching last page:', error);
        }
      };

      fetchData();
    }, [userId]);


    return (
      <View style={styles.container}>
          
          <View style={styles.header}>
            <StatusBar barStyle={'dark-content'} backgroundColor={'#f8f8ff'} />
            <TouchableOpacity style={{ marginTop: 5 }} onPress={handleBuku}>
              <Icon name="arrow-back" size={25} color="white" />
            </TouchableOpacity>
            <View style={styles.headerTextContainer}>
              <Text style={styles.headerText}>{subjudul}</Text>
              <View style={styles.downloadSearchContainer}>
                <TouchableOpacity style={styles.favoriteButton} onPress={() => addToFavorites(clickedTitle)}>
                  <Icon2 name="favorite" size={25} color="white" />
                </TouchableOpacity>
                <TouchableOpacity style={styles.downloadButton} onPress={() => handleDownload(clickedTitle)}>
                  <Icon name="download" size={25} color="white" />
                </TouchableOpacity>
                <TouchableOpacity onPress={handleSearch}>
                  <Icon name="search" size={25} color="white" />
                </TouchableOpacity>
              </View>
            </View>
          </View>


      

        <View>
          <Pdf
            trustAllCerts={false}
            source={source}
            page={lastViewedPage}
            onLoadComplete={(numberOfPages, filePath) => {
              console.log(`Number of pages: ${numberOfPages}`);
            }}
            onPageChanged={(page, numberOfPages) => onPageChanged(page, numberOfPages)}
            onError={error => {
              if (typeof error === 'string') {
                if (error.includes("Use of own trust manager but none defined")) {
                  // Handle SSL error
                  console.error("SSL error: " + error);
                } else {
                  // Handle other types of errors
                  console.error(error);
                }
              } else {
                // Handle non-string errors or do other appropriate actions
                console.error("Non-string error:", error);
              }
            }}
            
            onPressLink={uri => {
              console.log(`Link pressed: ${uri}`);
              // Handle link press, e.g., use Linking.openURL(uri)
            }}
            style={styles.pdf}
          />
        </View>
        <MenuBar />
      </View>
    );
  }

  const styles = StyleSheet.create({
    container: {
      flex: 1,
      justifyContent: 'flex-start',
      alignItems: 'center',
    },
    pdf: {
      flex: 1,
      width: Dimensions.get('window').width,
      height: Dimensions.get('window').height,
    },
    header: {
      flexDirection: 'row',
      alignItems: 'center',
      padding: 15,
      backgroundColor: 'red',
      borderBottomWidth: 1,
      borderBottomColor: 'red',
    },  
    headerText: {
      fontSize: 25,
      color: 'white',
      paddingLeft: 20,
      fontWeight: 'bold'
    },
    searchIcon: {
      marginHorizontal: 10,
    },
    headerTextContainer: {
      flex: 1,
      flexDirection: 'row',
      justifyContent: 'space-between',
      alignItems: 'center',
      paddingRight: 10, // Ubah dari marginRight ke paddingRight
    },
    downloadSearchContainer: {
      flexDirection: 'row',
      alignItems: 'center',
    },
    downloadButton: {
      marginRight: 20, // Sesuaikan margin agar lebih dekat
    },
    favoriteButton: {
      marginRight: 20, // Sesuaikan margin agar lebih dekat
    },
  });

  export default App;