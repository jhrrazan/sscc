import React, { useState, useEffect, useRef, useCallback } from 'react';
import { View, Text, TouchableOpacity, StatusBar, StyleSheet, Image, SafeAreaView } from 'react-native';
import AsyncStorage from '@react-native-async-storage/async-storage';
import { GestureHandlerRootView } from 'react-native-gesture-handler';
import { SafeAreaProvider } from 'react-native-safe-area-context';
import Icon from 'react-native-vector-icons/Ionicons';
import BottomSheet from '../components/bSheet';

const Edit = ({ navigation }) => {
  const [nama, setNama] = useState("");
  const [nik, setNik] = useState("");
  const bottomSheetRef = useRef(null);
  const [selectedImage, setSelectedImage] = useState(null);

  const expandHandler = useCallback(() => {
    bottomSheetRef.current.expand();
  }, []);

  // Data pengguna
  useEffect(() => {
    async function fetchData() {
      try {
        const authData = await AsyncStorage.getItem('authData');
        if (authData) {
          const parsedAuthData = JSON.parse(authData);

          if (parsedAuthData.nama && parsedAuthData.nik) {
            setNama(parsedAuthData.nama);
            setNik(parsedAuthData.nik);
          }
        }
      } catch (error) {
        console.error('Error fetching data:', error);
      }
    }
    fetchData();
  }, []);

  // (Image Profile) Membaca gambar yang dipilih dari AsyncStorage
  useEffect(() => {
    async function fetchData() {
      const imageUri = await AsyncStorage.getItem('selectedImage');
      setSelectedImage(imageUri);
    }
    fetchData();
  }, []);

  // Menangani perubahan pada gambar yang dipilih dari BottomSheet
  const handleImageSelected = (imageUri) => {
    setSelectedImage(imageUri);
  };

  const saveChangesHandler = async () => {
    try {
      if (selectedImage) {
        await AsyncStorage.setItem('selectedImage', selectedImage);
        console.log('Foto berhasil disimpan.');
      } else {
        console.log('Tidak ada foto untuk disimpan.');
      }
      navigation.navigate('Akun');
    } catch (error) {
      console.error('Error saving photo:', error);
    }
  };

  return (
    <SafeAreaProvider>
      <GestureHandlerRootView style={{ flex: 1 }}>
        <SafeAreaView style={{ flex: 1, backgroundColor: '#f8f8ff' }}>
          <StatusBar barStyle={'dark-content'} backgroundColor={'darkred'} />
          <View style={{ backgroundColor: 'darkred', paddingBottom: 15 }}>
            <View style={{
              flexDirection: 'row',
              justifyContent: 'space-between',
              alignItems: 'center',
              marginTop: 10,
            }}>
              <TouchableOpacity onPress={saveChangesHandler} style={{ flex: 1 }}>
                <Text style={{ fontSize: 16, color: '#ffffff', marginLeft: 10 }}>Kembali</Text>
              </TouchableOpacity>
              <View style={{ flex: 1, alignItems: 'center' }}>
                <Text style={{ fontSize: 20, color: '#ffffff', fontWeight: 'bold' }}>Edit profil</Text>
              </View>
              <View style={{ flex: 1 }}>
                {/* Untuk memberikan jarak di sebelah kanan */}
              </View>
            </View>
          </View>
          <View style={{ flex: 1, alignItems: 'center', marginTop: 20 }}>
            <View>
              {selectedImage && (
                <Image source={{ uri: selectedImage }} style={styles.imageProfile} />
              )}
            </View>
            <TouchableOpacity onPress={() => expandHandler()} style={{ marginTop: 15, flexDirection: 'row' }}>
              <Text style={{ color: 'black', fontSize: 15, fontWeight: 'bold', marginRight: 5 }}>Edit Foto</Text>
              <Icon name="create-outline" size={20} color="black" />
            </TouchableOpacity>
            <View style={{ marginTop: 50, alignItems: 'center' }}>
              <Text style={{ color: 'black', fontWeight: 'bold', fontSize: 18 }}>Hi</Text>
              <Text style={{ color: 'black', fontWeight: 'bold', fontSize: 18 }}>{nama}</Text>
              <Text style={{ color: 'black' }}>{nik}</Text>
            </View>
          </View>
          <BottomSheet ref={bottomSheetRef} snapTo={'75%'} onImageSelected={handleImageSelected} />
        </SafeAreaView>
      </GestureHandlerRootView>
    </SafeAreaProvider>
  );
}

const styles = StyleSheet.create({
  imageProfile: {
    width: 125,
    height: 125,
    marginTop: 30,
    borderRadius: 70,
    borderColor: 'black',
    borderWidth: 1,
  },
  image: {
    width: '100%',
    height: '100%',
    borderRadius: 70,
  },
  imagePlaceholder: {
    color: 'gray',
    textAlign: 'center',
    fontSize: 16,
  },
});

export default Edit;