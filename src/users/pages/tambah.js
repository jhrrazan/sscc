import React, { useState } from 'react';
import { View, Text, TouchableOpacity, StatusBar, TextInput, StyleSheet } from 'react-native';
import DocumentPicker from 'react-native-document-picker';
import Icon from 'react-native-vector-icons/Ionicons';
import FlatButton from './button';
import MenuBar from '../../template/MenuBar';

const App = () => {
  const [selectedFile, setSelectedFile] = useState(null);

  const pickDocument = async () => {
    try {
      const result = await DocumentPicker.pick({
        type: [DocumentPicker.types.pdf],
      });
      console.log(result);

      // Store the selected file for further use if needed
      setSelectedFile(result);
    } catch (err) {
      if (DocumentPicker.isCancel(err)) {
        // User cancelled the picker
        console.log('User cancelled the picker');
      } else {
        throw err;
      }
    }
  };

  return (
    <View style={{ flex: 1, backgroundColor: '#ffffff' }}>
      <View style={{ flex: 1 }}>
        <View style={{
          marginHorizontal: 20,
          flexDirection: 'row',
          margin: 10,
        }}>
          <StatusBar barStyle={'dark-content'} backgroundColor={'#f8f8ff'} />
          <TouchableOpacity style={{ marginTop: 5 }}>
            <Icon name="menu-outline" size={25} color="black" />
          </TouchableOpacity>
          <Text style={{ fontSize: 25, color: 'crimson', paddingLeft: 20 }}>Tambah Buku</Text>
        </View>

        <TextInput
          style={styles.textInput}
          placeholder='Title'
        />

        {/* Add more TextInput components for other information */}

        <TouchableOpacity style={styles.button} onPress={pickDocument}>
          <Text style={{ color: 'white', textAlign: 'center' }}>Select PDF</Text>
        </TouchableOpacity>

        {selectedFile && (
          <Text style={{ margin: 10 }}>
            Selected File: {selectedFile.name}
          </Text>
        )}

        <FlatButton text='Save' onPress={styles.handleSubmit} />
      </View>

      {/* Move MenuBar to the bottom */}
      <MenuBar />
    </View>
  );
};

const styles = StyleSheet.create({
  textInput: {
    fontSize: 18,
    color: 'grey',
    borderWidth: 2,
    borderColor: 'grey',
    marginLeft: 20,
    marginRight: 20,
    margin: 10,
  },
  button: {
    borderRadius: 10,
    paddingVertical: 10,
    paddingHorizontal: 10,
    backgroundColor: 'maroon',
    marginLeft: 20,
    marginRight: 20,
    margin: 10,
  },
});

export default App;
