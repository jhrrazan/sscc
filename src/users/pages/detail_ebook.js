import React, { useState } from 'react';
import { View, Text, TouchableOpacity, StatusBar, Image, Dimensions, StyleSheet, FlatList } from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';
import MenuBar from '../../template/MenuBar';

const { width } = Dimensions.get('window');

const data = [
    { id: '1', text: 'Ebook 1', imageSource: require('../../asset/images/sample.jpg'), author: 'Author Name 1', description: 'Description 1' },
    // Add more data as needed
];

const App = ({ navigation }) => {
    const [isLoved, setIsLoved] = useState(false);

    const handlePress = (item) => {
        navigation.navigate('BookDetail', { item });
    };

    const toggleLove = () => {
        setIsLoved(!isLoved);
    };

    return (
        <View style={{ flex: 1, backgroundColor: '#ffffff', alignItems: 'center' }}>
            <View style={{
                marginHorizontal: 20,
                flexDirection: 'row',
                alignItems: 'center',
                margin: 10,
            }}>
                <StatusBar barStyle={'dark-content'} backgroundColor={'#f8f8ff'} />
                <TouchableOpacity style={{ marginTop: 5 }} >
                    <Icon name="arrow-back" size={25} color="black" />
                </TouchableOpacity>
                <View style={{ flex: 1, marginLeft: 20 }}>
                    <Text style={{ fontSize: 25, color: 'crimson' }}>Detail Ebook</Text>
                </View>
                {/* Love Button */}
                <TouchableOpacity
                    onPress={toggleLove}
                    style={{ position: 'absolute', right: 20, top: 10 }}
                >
                    <Icon
                        name={isLoved ? 'heart' : 'heart-outline'}
                        size={25}
                        color={isLoved ? 'red' : 'black'}
                        style={{ backgroundColor: 'transparent' }}
                    />
                </TouchableOpacity>
            </View>

            <FlatList
                data={data}
                keyExtractor={(item) => item.id}
                renderItem={({ item }) => (
                    <View style={{ alignItems: 'center' }}>
                        <Image
                            source={item.imageSource}
                            style={{ width: width * 0.3, height: width * 0.5, resizeMode: 'cover', borderRadius: 20, overflow: 'hidden', margin: 20 }}
                        />
                        <View style={styles.buttonContainer}>
                            <TouchableOpacity style={styles.button} onPress={() => console.log('Download pressed')}>
                                <View style={styles.iconContainer}>
                                    <Icon name="download" size={20} color="white" style={styles.searchIcon} />
                                    <Text style={styles.buttonText}>Download</Text>
                                </View>
                            </TouchableOpacity>
                            {/* Add space between buttons */}
                            <View style={{ width: 10 }} />
                            <TouchableOpacity style={styles.button} onPress={() => console.log('Favorite pressed')}>
                                <View style={styles.iconContainer}>
                                    <Icon name="reader-outline" size={20} color="white" style={styles.searchIcon} />
                                    <Text style={styles.buttonText}>Baca Buku</Text>
                                </View>
                            </TouchableOpacity>
                        </View>
                    </View>
                )}
                horizontal
                showsHorizontalScrollIndicator={false}
            />
            
            

            <MenuBar />
        </View>
    );
};

const styles = StyleSheet.create({
    buttonContainer: {
        flexDirection: 'row',
        justifyContent: 'center', // Adjust the justification if needed
        marginTop: 10,
    },
    button: {
        backgroundColor: 'crimson',
        padding: 10,
        borderRadius: 8,
    },
    buttonText: {
        color: 'white',
        fontSize: 16,
        marginLeft: 5, // Adjust the margin if needed
    },
    iconContainer: {
        flexDirection: 'row',
        alignItems: 'center',
    },
    searchIcon: {
        marginRight: 5, // Adjust the margin if needed
    },
});

export default App;
