import React from 'react';
import { View, Text, StyleSheet, TouchableOpacity } from 'react-native';
import Pdf from 'react-native-pdf';
import RNFS from 'react-native-fs';
import Icon from 'react-native-vector-icons/Ionicons';
import { useNavigation } from '@react-navigation/native';
import {check, PERMISSIONS, RESULTS} from 'react-native-permissions';

const App = ({ route }) => {
  const navigation = useNavigation();
  const { book, path, text, title, subjudul } = route.params; // Receiving book information and path from navigation

  console.log('Received title:', title);

  const backPress = () => {
    navigation.navigate('Download');
  };

  const source = { uri: `file://${path}`, cache: true };

  check(PERMISSIONS.IOS.LOCATION_ALWAYS)
  .then((result) => {
    switch (result) {
      // case RESULTS.UNAVAILABLE:
      //   console.log('This feature is not available (on this device / in this context)');
      //   break;
      case RESULTS.DENIED:
        console.log('The permission has not been requested / is denied but requestable');
        break;
      case RESULTS.LIMITED:
        console.log('The permission is limited: some actions are possible');
        break;
      case RESULTS.GRANTED:
        console.log('The permission is granted');
        break;
      case RESULTS.BLOCKED:
        console.log('The permission is denied and not requestable anymore');
        break;
    }
  })
  .catch((error) => {
    // …
  });

  // Menentukan panjang maksimum judul yang diizinkan
  const maxTitleLength = 20; // Ubah angka sesuai kebutuhan

  // Mengatur judul yang sudah dipotong jika melebihi panjang maksimum
  const displayedTitle = title.length > maxTitleLength
  ? title.substring(0, maxTitleLength - 3) + '...' // Menambahkan ellipsis jika dipotong
  : title;

  return (
    <View style={styles.container}>
      <View style={styles.header}>
        <TouchableOpacity style={styles.backButton} onPress={backPress}>
          <Icon name="arrow-back" size={25} color="white" />
        </TouchableOpacity>
        <Text style={styles.headerText} numberOfLines={1} ellipsizeMode="tail">{title}</Text>
      </View>
      <Pdf
        source={source}
        password='123'
        onLoadComplete={(numberOfPages, filePath) => {
          console.log(`Number of pages: ${numberOfPages}`);
        }}
        onError={(error) => {
          console.error('Error displaying PDF:', error);
        }}
        style={{ flex: 1, width: '100%', height: '100%' }}
      />
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white',
  },
  header: {
    flexDirection: 'row',
    alignItems: 'center',
    padding: 15,
    // paddingVertical: 10,
    paddingHorizontal: 20,
    backgroundColor: 'red',
    borderBottomWidth: 1,
    borderBottomColor: 'red',
  },
  backButton: {
    padding: 10,
  },
  headerText: {
    fontSize: 25,
    color: 'white',
    fontWeight: 'bold',
    marginLeft: 20,
    maxWidth: '90%', // Atur maksimum lebar teks untuk header
  },
});

export default App;
