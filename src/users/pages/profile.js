import React, { useState, useEffect } from 'react';
import { View, Text, TouchableOpacity, StatusBar, StyleSheet, Image, KeyboardAvoidingView } from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';
import { SafeAreaView } from 'react-native-safe-area-context';
import { useNavigation } from '@react-navigation/native';

const Profile = ({ navigation }) => {

  const backPress = () => {
    navigation.navigate('Home');
  }

  return (
    <SafeAreaView style={{ flex: 1, backgroundColor: '#f8f8ff' }}>
      <KeyboardAvoidingView style={{ flex: 1, }}>
        <View style={{
          marginHorizontal: 20,
          flexDirection: 'row',
          marginTop: 10,
        }}>
          <StatusBar barStyle={'dark-content'} backgroundColor={'#f8f8ff'} />
          <TouchableOpacity style={{ marginTop: 5 }} onPress={backPress}>
            <Icon name="arrow-back" size={25} color="black" />
          </TouchableOpacity>
          <Text style={{ fontSize: 25, color: 'black', paddingLeft: 20, fontWeight: '500' }}>Profile</Text>
        </View>
        <View style={{ flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center', marginTop: 10 }}>
          <Image source={require('../../asset/images/logoTelkomsat.png')} style={{ height: 50, width: 180, marginLeft: 3 }} />
          <Image source={require('../../asset/images/Picture1.png')} style={{ height: 50, width: 50, marginRight: 5 }} />
        </View>
        <View style={{ alignItems: 'center' }}>
          <Text style={styles.header}>Profile PT. TELKOMSAT</Text>
        </View>
        <View style={styles.containerHeader}>
          <Text style={styles.textP}>PT. Telkom Satelit Indonesia atau yang biasa disebut oleh masyarakat publik dengan sebutan TELKOMSAT ini adalah sebuah
            perusahaan yang memiliki titik fokus bidang yang bergerak dalam jasa layanan khususnya telekomunikasi satelit serta mencakup terestrial.
            Perusahaan ini memberikan layanan dan fasilitas dalam menyediakan telekomunikasi yang diperuntukkan khususnya untuk berbagai perusahaan - perusahaan
            yang sudah tergabung dalam layanananya, jadi mereka memberikan layanan bukan untuk publik.  </Text>
          <Text style={styles.textP2}>Selain itu mereka juga melakukan penyelenggaraan telekomunikasi
            jaringan tertutup. Dengan nama besar Telkomsat ini mereka sudah cukup dikenal, memberikan layanan berupa fasilitas telekomunikasi yang mana layanannya juga
            mencakup telekomunikasi seperti jaringan komunikasi data, suara, video, multimedia dan internet berdasarkan satelit. Ketangguhan perusahaan ini juga semakin
            diakui dengan adanya lisensi dalam bidang Penyelenggaraan jaringan tetap tertutup, penyelenggaraan jasa akses internet, Network Access Poin (NAP),
            penyelenggaraan jasa interkoneksi, dan SISKOMDAT atau penyelenggaraan jasa Sistem Komunikasi Data.</Text>

        </View>
      </KeyboardAvoidingView>
    </SafeAreaView>
  );
}

const styles = StyleSheet.create({
  containerHeader: {
    flex: 1,
    marginHorizontal: 20,
    marginVertical: 10,
    marginTop: 25,
    alignItems: 'flex-start',
  },
  header: {
    fontSize: 20,
    color: 'black',
    marginTop: 20,
  },
  textP: {
    fontSize: 16,
    color: 'black',
  },
  textP2: {
    fontSize: 16,
    color: 'black',
    marginTop: 10
  },
});
export default Profile;