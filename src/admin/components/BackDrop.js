import React from 'react';
import { StyleSheet, TouchableWithoutFeedback, View } from 'react-native';
import Animated, { interpolate, useAnimatedStyle } from 'react-native-reanimated';

const BackDrop = ({ topAnimation, openHeight, closeHeight, close }) => {
  const backDropAnimation = useAnimatedStyle(() => {
    const opacity = interpolate(topAnimation.value, [closeHeight, openHeight], [0, 0.5]);
    const display = opacity === 0 ? 'none' : 'flex';
    return {
      opacity,
      display,
    };
  });

  return (
    <TouchableWithoutFeedback onPress={close}>
      <Animated.View style={[styles.container, backDropAnimation]}></Animated.View>
    </TouchableWithoutFeedback>
  );
};

const styles = StyleSheet.create({
  container: {
    ...StyleSheet.absoluteFillObject,
    backgroundColor: 'black',
    display: 'none',
  },
});

export default BackDrop;
