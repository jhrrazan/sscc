import React, { forwardRef, useCallback, useImperativeHandle, useState } from 'react';
import { StyleSheet, Text, View, TouchableOpacity, Dimensions } from 'react-native';
import Animated, { useSharedValue, useAnimatedStyle, withTiming } from 'react-native-reanimated';
import Icon from 'react-native-vector-icons/Ionicons';
import BackDrop from './BackDrop';
import AsyncStorage from '@react-native-async-storage/async-storage';
import { launchImageLibrary, launchCamera } from 'react-native-image-picker';

const BottomSheet = forwardRef((props, ref) => {
  const { snapTo } = props;
  const height = Dimensions.get('screen').height;
  const closeHeight = height;
  const percentage = parseFloat(snapTo.replace('%', '')) / 100;
  const openHeight = height * percentage;
  const topAnimation = useSharedValue(closeHeight);
  const [selectedImage, setSelectedImage] = useState(null);

  const expand = useCallback(() => {
    'worklet'
    topAnimation.value = withTiming(openHeight);
  }, [openHeight, topAnimation]);

  const close = useCallback(() => {
    'worklet'
    topAnimation.value = withTiming(closeHeight);
  }, [closeHeight, topAnimation]);

  const animationStyle = useAnimatedStyle(() => {
    const top = topAnimation.value;
    return {
      top,
    };
  });

  const openGallery = () => {
    const options = {
      mediaType: 'photo',
      includeBase64: false,
      maxHeight: 2000,
      maxWidth: 2000,
    };

    launchImageLibrary(options, async (response) => {
      if (response.didCancel) {
        console.log('User cancelled image picker');
      } else if (response.error) {
        console.log('Image picker error: ', response.error);
      } else {
        let imageUri = response.uri || response.assets?.[0]?.uri;
        setSelectedImage(imageUri); // Setel ulang state dengan URL gambar baru
        props.onImageSelected(imageUri);

        try {
          // Simpan alamat gambar ke AsyncStorage
          await AsyncStorage.setItem('selectedImage', imageUri);
        } catch (error) {
          console.error('Error saving image to AsyncStorage:', error);
        }

        close();
      }
    });
  };

  const openCamera = () => {
    const options = {
      mediaType: 'photo',
      includeBase64: false,
      maxHeight: 2000,
      maxWidth: 2000,
    };

    launchCamera(options, async (response) => {
      if (response.didCancel) {
        console.log('User cancelled camera');
      } else if (response.error) {
        console.log('Camera error: ', response.error);
      } else {
        let imageUri = response.uri || response.assets?.[0]?.uri;
        setSelectedImage(imageUri); // Perbarui state dengan URL gambar baru]
        props.onImageSelected(imageUri);


        try {
          // Simpan alamat gambar ke AsyncStorage
          await AsyncStorage.setItem('selectedImage', imageUri);
        } catch (error) {
          console.error('Error saving image to AsyncStorage:', error);
        }

        close();
      }
    });
  };

  useImperativeHandle(ref, () => ({
    expand,
    close,
    selectedImage,
  }), [expand, close, selectedImage]);

  return (
    <>
      <BackDrop
        topAnimation={topAnimation}
        closeHeight={closeHeight}
        openHeight={openHeight}
        close={close}
      />
      <Animated.View style={[
        styles.container,
        animationStyle,
      ]}>
        <View style={styles.lineContainer}>
          <View style={styles.line} />
        </View>
        <View>
          <TouchableOpacity onPress={openGallery} style={{ flexDirection: 'row', marginLeft: 15, marginTop: 17 }}>
            <Icon name="image-outline" size={28} color="black" />
            <Text style={styles.textContainer}>Pilih Dari Galeri</Text>
          </TouchableOpacity>
          <TouchableOpacity onPress={openCamera} style={{ flexDirection: 'row', marginLeft: 15, marginTop: 17 }}>
            <Icon name="camera-outline" size={28} color="black" />
            <Text style={styles.textContainer}>Ambil Foto</Text>
          </TouchableOpacity>
        </View>
      </Animated.View>
    </>
  );
});

const styles = StyleSheet.create({
  container: {
    ...StyleSheet.absoluteFillObject,
    backgroundColor: '#ffffff',
    borderTopLeftRadius: 20,
    borderTopRightRadius: 20,
  },
  lineContainer: {
    alignItems: 'center',
    marginVertical: 15,
  },
  line: {
    width: 45,
    height: 4,
    backgroundColor: 'black',
    borderRadius: 10,
  },
  textContainer: {
    color: 'black',
    fontSize: 15,
    marginLeft: 10,
    marginTop: 3.5,
  },
});

export default BottomSheet;
