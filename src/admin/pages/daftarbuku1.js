import React, { useState, useEffect } from 'react';
import { View, Text, TouchableOpacity, StatusBar, Image, Dimensions, ScrollView, FlatList, StyleSheet, TextInput, ImageBackground } from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';


const { width } = Dimensions.get('window');

const App = () => {
    return (
        <View style={{ flex: 1, backgroundColor: '#ffffff' }} >
            <View style={{ flex: 1, backgroundColor: '#ffffff' }}>
                <View style={{
                    flexDirection: 'row',
                    width: width * 1,
                    height: width * 0.5,
                    backgroundColor: 'lightgray',
                    justifyContent: 'center',
                    alignItems: 'center',
                    borderBottomRightRadius: 20,
                    borderBottomLeftRadius: 20,
                }}>
                    <StatusBar barStyle={'dark-content'} backgroundColor={'#f8f8ff'} />
                    <ImageBackground
                        source={require('../../asset/images/bgdaftar.jpeg')}
                        style={{ width: '100%', height: '100%', resizeMode: 'cover', borderRadius: 20, overflow: 'hidden' }}
                    >
                        <View style={styles.searchContainer}>
                            <Icon name="search" size={20} color="grey" style={styles.searchIcon} />
                            <TextInput style={styles.textInput} placeholder='Cari Buku' />
                        </View>
                    </ImageBackground>
                </View>

                <View style={{
                    marginHorizontal: 20,  
                    flexDirection: 'row', 
                    marginTop: 10 ,
                }}>
                    <StatusBar barStyle={'dark-content'} backgroundColor={'#f8f8ff'}/>
                    <Text style={{fontSize: 25, color: 'black', paddingLeft: 0 }}>Daftar Buku</Text>
                </View>

                <TouchableOpacity>
                <View style={{
                    marginHorizontal: 20,  
                    flexDirection: 'row',
                    justifyContent: 'space-between', // Ini akan menempatkan teks di kiri dan ikon di kanan
                    alignItems: 'center', // Ini akan mengatur kedua elemen secara vertikal 
                    marginTop: 10 ,
                    }}>
                    <Text style={{fontSize: 20, color: 'black', paddingLeft: 0 }}>Ebook 1</Text>
                    <Icon name="arrow-forward" size={20} color="black" />
                </View>
                </TouchableOpacity>
                <View style={styles.divider} />

                <TouchableOpacity>
                <View style={{
                    marginHorizontal: 20,  
                    flexDirection: 'row',
                    justifyContent: 'space-between', // Ini akan menempatkan teks di kiri dan ikon di kanan
                    alignItems: 'center', // Ini akan mengatur kedua elemen secara vertikal 
                    marginTop: 10 ,
                    }}>
                    <Text style={{fontSize: 20, color: 'black', paddingLeft: 0 }}>Ebook 2</Text>
                    <Icon name="arrow-forward" size={20} color="black" />
                </View>
                </TouchableOpacity>
                <View style={styles.divider} />

                <TouchableOpacity>
                <View style={{
                    marginHorizontal: 20,  
                    flexDirection: 'row',
                    justifyContent: 'space-between', // Ini akan menempatkan teks di kiri dan ikon di kanan
                    alignItems: 'center', // Ini akan mengatur kedua elemen secara vertikal 
                    marginTop: 10 ,
                    }}>
                    <Text style={{fontSize: 20, color: 'black', paddingLeft: 0 }}>Ebook 3   </Text>
                    <Icon name="arrow-forward" size={20} color="black" />
                </View>
                </TouchableOpacity>
                <View style={styles.divider} />
            </View>
            
        </View>
        
    );
}

const styles = StyleSheet.create({
    // ... existing styles ...

    searchContainer: {
        flexDirection: 'row',
        alignItems: 'center',
        backgroundColor: '#bcbcbc',
        height: 40,
        borderRadius: 10,
        margin: 25,
        marginTop: 125,
        paddingLeft: 10,
    },
    searchIcon: {
        marginRight: 10,
    },
    textInput: {
        flex: 1,
        fontSize: 17,
        color: 'grey',
    },
    divider: {
        height: StyleSheet.hairlineWidth,
        backgroundColor: '#A9ABB1',
        marginHorizontal: 16,
        marginTop: 16,
    },

    // ... existing styles ...
});

export default App;
