import React, { useState, useEffect } from 'react';
import { View, Text, TouchableOpacity, StatusBar, FlatList, StyleSheet, Alert } from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';
import MenuBar from '../../template/MenuBar';
import { useNavigation } from '@react-navigation/native';
import AsyncStorage from '@react-native-async-storage/async-storage'; // Import AsyncStorage
import axios from 'axios';

const App = () => {
  const navigation = useNavigation();
  const [favoriteTitles, setFavoriteTitles] = useState([]); // State untuk daftar judul favorit
  const [clickedTitles, setClickedTitles] = useState([]);
  const [selectedEbook, setSelectedEbook] = useState(null);
  const [bukuList, setBukuList] = useState([]);
  const uniqueClickedTitles = Array.from(new Set(clickedTitles)); // Dapatkan judul unik
  const sortedUniqueClickedTitles = uniqueClickedTitles.sort(); // Sort judul untuk tampilan yang teratur
  const [userId, setUserId] = useState(null);

  const getUserIdFromStorage = async () => {
    try {
      const storedUserId = await AsyncStorage.getItem("userId");
      if (storedUserId) {
        setUserId(storedUserId); // Set userId ke state jika berhasil diambil
        console.log("UserID:", storedUserId); // Panggil console.log disini
      }
    } catch (error) {
      console.error("Error retrieving userId:", error);
    }
  };
  
  // Panggil fungsi untuk mengambil userId saat komponen dimount
  useEffect(() => {
    getUserIdFromStorage();
  }, []);

  useEffect(() => {
    axios.get('http://192.168.100.7:3001/api/getSubjudul')
      .then(response => {
        console.log('Data yang diterima:', response.data); // Tambahkan ini untuk melihat respons dari server
        setBukuList(response.data);
      })
      .catch(error => {
        console.log(error);
      });
  }, []);

  // const ebooks = bukuList && bukuList.map(buku => (
  //   { title: buku.title, name: buku.name, id: buku._id, path: buku.path, subjudul }
  // ));

  const bukuListWithSubjudul = bukuList && bukuList.map(buku => ({
    subjudul: buku.subjudul,
    id: buku._id,
    path: buku.path,
    name: buku.name
  }));

  useEffect(() => {
    const fetchData = async () => {
      try {
        if (userId) {
          const storedTitles = await AsyncStorage.getItem(`favoriteTitles_${userId}`);
          if (storedTitles) {
            setFavoriteTitles(JSON.parse(storedTitles));
          }
        } else {
          console.log("User ID favorite not found");
        }
      } catch (error) {
        console.error("Error fetching favorite titles:", error);
        // Handle error if needed
      }
    };
  
    fetchData();
  }, [userId]);

  useEffect(() => {
    const getLastViewedTitles = async () => {
      try {
        const value = await AsyncStorage.getItem(`clickedTitles_${userId}`);
        if (value !== null) {
          setClickedTitles(JSON.parse(value));
        }
      } catch (error) {
        console.error('Error retrieving clicked titles: ', error);
      }
    };

    getLastViewedTitles();
  }, [userId]);

  const backPress = () => {
    navigation.navigate('Home');
  };

  const handleBuku = (favoriteTitle) => {
    const selectedBook = bukuList.find(buku => {
      if (typeof buku.subjudul === 'string' && typeof favoriteTitle === 'string') {
        return buku.subjudul.includes(favoriteTitle) || favoriteTitle.includes(buku.subjudul);
      }
      return false;
    });
  
    if (selectedBook) {
      const { subjudul, title, name, _id, path } = selectedBook;
      const selectedTitle = selectedEbook ? selectedEbook.title : '';
      const clickedTitleEntry = `${selectedTitle} - ${subjudul}`;
  
      console.log('Selected Book:', selectedBook);
      console.log('Selected Title:', selectedTitle);
      console.log('Clicked Title Entry:', clickedTitleEntry);
      console.log('Selected Book ID:', _id);
  
      let updatedFavoriteTitles = favoriteTitles.filter((favTitle) => favTitle !== favoriteTitle);
      updatedFavoriteTitles = [favoriteTitle, ...updatedFavoriteTitles.slice(0, 4)];
  
      setFavoriteTitles(updatedFavoriteTitles);
  
      AsyncStorage.setItem(`favoriteTitles_${userId}`, JSON.stringify(updatedFavoriteTitles))
        .then(() => {
          console.log('Favorite titles saved:', updatedFavoriteTitles);
        })
        .catch((error) => {
          console.error('Error saving favorite titles:', error);
        });
  
      setSelectedEbook(selectedBook);
  
      navigation.navigate('PDFViewer_Favorite', { title: selectedTitle, subjudul, name, id: _id, path, clickedEntry: clickedTitleEntry, favoriteTitle });
    }
  };
  
  
  

  // // navigasi buku
  // const handleBuku = (favoriteTitles) => {
  //   // ambil judul buku dan isinya
  //   const selectedSubjudul = bukuList.find(buku => {
  //     if (typeof buku.subjudul === 'string' && typeof favoriteTitles === 'string') {
  //       // Menggunakan includes() untuk mencari kesamaan kalimat
  //       return buku.subjudul.includes(favoriteTitles) || favoriteTitles.includes(buku.subjudul);
  //     }
  //     return false;
  //   });

  //   if (selectedSubjudul) {
  //     const { subjudul, title, name, _id, path } = selectedSubjudul;
  //     const selectedTitle = selectedEbook ? selectedEbook.title : '';
  //     const clickedTitleEntry = `${selectedTitle} - ${subjudul}`;
  //     // Log ID yang dikirim
  //     console.log('Selected Subjudul ID:', _id);
  
  //     // Filter entri yang tidak sama dengan entri yang baru
  //     let updatedClickedTitles = clickedTitles.filter((clicked) => clicked !== favoriteTitles);

  //     // Tambahkan entri baru ke array
  //     updatedClickedTitles = [favoriteTitles, ...updatedClickedTitles.slice(0, 4)]; // Menambahkan baru di awal, membatasi panjang array

  //     setClickedTitles(updatedClickedTitles);
  
  //     // Simpan ke AsyncStorage
  //     AsyncStorage.setItem('clickedTitles', JSON.stringify(updatedClickedTitles))
  //       .then(() => {
  //         console.log('Clicked titles saved:', updatedClickedTitles);
  //       })
  //       .catch((error) => {
  //         console.error('Error saving clicked titles:', error);
  //       });

  //     // Set the selected eBook
  //     setSelectedEbook(selectedSubjudul);

  //     // Navigasi ke layar PDFViewer dengan judul buku dan subjudul
  //     navigation.navigate('PDFViewer', { title: selectedTitle, subjudul, name, id: _id, path });
  //   }
  // };

  const removeFromFavorites = async (title) => {
  Alert.alert(
    'Remove from Favorites',
    `Are you sure you want to remove "${title}" from favorites?`,
    [
      {
        text: 'Cancel',
        style: 'cancel',
      },
      {
        text: 'Remove',
        style: 'destructive',
        onPress: async () => {
          try {
            const updatedTitles = favoriteTitles.filter(item => item !== title);
            setFavoriteTitles(updatedTitles);
            await AsyncStorage.setItem(`favoriteTitles_${userId}`, JSON.stringify(updatedTitles));
            Alert.alert('Success', `${title} removed from favorites!`);
          } catch (error) {
            Alert.alert('Error', 'Failed to remove from favorites');
          }
        },
      },
    ],
    { cancelable: false }
  );
};


  const renderFavoriteItem = ({ item }) => (
    <TouchableOpacity onPress={() => handleBuku(item)}>
      <View style={styles.bookItemContainer}>
        <View style={styles.bookDetails}>
          <Text style={styles.bookTitle}>{item}</Text>
        </View>
        <TouchableOpacity onPress={() => removeFromFavorites(item)} style={styles.deleteButton}>
          <Icon name="trash-outline" size={20} color="red" />
        </TouchableOpacity>
      </View>
      <View style={styles.separator} />
    </TouchableOpacity>
  );
  

  return (
    <View style={styles.container}>
      <View style={styles.header}>
        <TouchableOpacity style={styles.backButton} onPress={backPress}>
          <Icon name="arrow-back" size={25} color="white" />
        </TouchableOpacity>
        <Text style={styles.headerText}>Favorite</Text>
      </View>

      {favoriteTitles.length > 0 ? (
        <FlatList
          data={favoriteTitles}
          keyExtractor={(item, index) => `${item}_${index}`}
          renderItem={renderFavoriteItem}
        />
      ) : (
        <View style={styles.noFavoritesContainer}>
          <Text style={styles.noFavoritesText}>No favorites yet</Text>
        </View>
      )}

      <MenuBar />
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white',
  },
  header: {
    flexDirection: 'row',
    alignItems: 'center',
    paddingVertical: 15,
    paddingHorizontal: 20,
    backgroundColor: 'maroon',
    borderBottomWidth: 1,
    borderBottomColor: 'maroon',
  },
  backButton: {
    padding: 10,
  },
  bookItem: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    padding: 16,
    borderBottomWidth: 1,
    borderBottomColor: '#ccc',
  },
  headerText: {
    fontSize: 25,
    color: 'white',
    fontWeight: 'bold',
    marginLeft: 20,
  },
  bookItemContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    padding: 16,
  },
  bookDetails: {
    flex: 1,
  },
  bookTitle: {
    fontSize: 18,
    fontWeight: 'bold',
    marginRight: 16,
  },
  noFavoritesContainer: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  noFavoritesText: {
    textAlign: 'center',
    marginTop: 20,
    fontSize: 18,
    color: 'gray',
  },
  deleteButton: {
    padding: 10,
  },
  separator: {
    height: 1,
    backgroundColor: '#ccc',
    // marginHorizontal: 20,
  },
});

export default App;
