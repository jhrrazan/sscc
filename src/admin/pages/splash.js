import React, {useEffect} from "react";
import { View, Image, StyleSheet, Text, ImageBackground } from 'react-native';
import { useNavigation } from '@react-navigation/native';

const Splash = () => {

    const navigation = useNavigation();

    useEffect(() => {
        
        setTimeout(() => {
          navigation.navigate('Login');
        }, 800);
      }, []);

    return (
        <ImageBackground source={require('../../asset/images/splash.jpg')} style={styles.BackgroundImage}>
            <View  style={styles.container} >
                <Image source={require('../../asset/images/img2.png')}/>
            </View>
        </ImageBackground>
        
    );
};

const styles = StyleSheet.create({
    BackgroundImage: {
      flex: 1,
      resizeMode: 'cover',
    },
    container: {
        flex: 1,
      justifyContent: 'center',
      alignItems: 'center',
      backgroundColor: 'rgba(0,0,0,0.5)',
    },
    
});

export default Splash;