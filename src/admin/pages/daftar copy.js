import React, { useState, useEffect } from 'react';
import {
  View,
  Text,
  TouchableOpacity,
  StatusBar,
  Image,
  Dimensions,
  StyleSheet,
  TextInput,
  ImageBackground,
  ScrollView,
  Modal,
  TouchableWithoutFeedback,
  Keyboard,
  Alert,
  FlatList,
  KeyboardAvoidingView,
  Platform
} from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';
import MenuBar from '../../template/MenuBar';
import axios from 'axios';
import DocumentPicker from 'react-native-document-picker';
import AsyncStorage from '@react-native-async-storage/async-storage';

const { width, height } = Dimensions.get('window');

const App = ({ navigation }) => {
  const [selectedEbook, setSelectedEbook] = useState(null);
  const [selectedEbookIndex, setSelectedEbookIndex] = useState(null);
  const [showButtons, setShowButtons] = useState(false);
  const [searchText, setSearchText] = useState('');
  const [bukuList, setBukuList] = useState([]);
  const [modalVisible, setModalVisible] = useState(false);
  const [newBookName, setNewBookName] = useState('');
  const [newSubjudulName, setNewSubjudulName] = useState('');
  const [halaman, setHalaman] = useState('');
  const [title, setTitle] = useState('');
  const [bab, setBab] = useState('');
  const [pdfUri, setPdfUri] = useState('');
  const [loading, setLoading] = useState(false);
  const [error, setError] = useState('');
  const [fileResponse, setFileResponse] = useState([]);
  const [lastViewedTitle, setLastViewedTitle] = useState('');
  const [selectedTitle, setSelectedTitle] = useState(null); // Add new state variable
  const [clickedTitles, setClickedTitles] = useState([]);
  const [renameModalVisible, setRenameModalVisible] = useState(false);
  const [renameSubjudulModalVisible, setRenameSubjudulModalVisible] = useState(false);
  const [newbookModalVisible, setNewbookModalVisible] = useState(false)
  const [selectedBookForRename, setSelectedBookForRename] = useState(null);
  const [selectedSubjudulForRename, setSelectedSubjudulForRename] = useState(null);
  const [favoriteTitles, setFavoriteTitles] = useState([]); // State untuk daftar judul favorit
  const [downloadedBooks, setDownloadedBooks] = useState([]);
  const [subjudulList, setSubjudulList] = useState([]);
  const [subjudul, setSubjudul] = useState(''); // Contoh penggunaan useState untuk menginisialisasi state subjudul
  const [subjudulId, setSubjudulId] = useState([]); 
  const [selectedSubjudul, setSelectedSubjudul] = useState(null);
  const [judul, setJudul] = useState(''); // Contoh penggunaan useState untuk menginisialisasi state subjudul
  const [userId, setUserId] = useState(null);

  const getUserIdFromStorage = async () => {
    try {
      const storedUserId = await AsyncStorage.getItem("userId");
      if (storedUserId) {
        setUserId(storedUserId); // Set userId ke state jika berhasil diambil
        console.log("UserID:", storedUserId); // Panggil console.log disini
      }
    } catch (error) {
      console.error("Error retrieving userId:", error);
    }
  };
  
  // Panggil fungsi untuk mengambil userId saat komponen dimount
  useEffect(() => {
    getUserIdFromStorage();
  }, []);


  useEffect(() => {
    axios.get('http://192.168.100.7:3001/api/getAll')
      .then(response => {
        setBukuList(response.data);
      })
      .catch(error => {
        console.log(error);
      });
  }, []);

  const ebooks = bukuList && bukuList.map(buku => (
    { title: buku.title, name: buku.name, id: buku._id, path: buku.path, subjudul: buku.subjudul }
  ));

  // Search buku
  const filteredEbooks = ebooks.filter(ebook => ebook.title && ebook.title.toLowerCase().includes(searchText.toLowerCase()));

  const handleEbookPress = (ebook, index) => {
    if (selectedEbook && selectedEbook.title === ebook.title) {
      setShowButtons(!showButtons);
    } else {
      setSelectedEbook(ebook);
      setSelectedSubjudul(ebook);
      setSelectedEbookIndex(index);
      setShowButtons(true);
      setSubjudulId(ebook.id); // Menyimpan ID buku terkait
  
      const selectedTitle = ebook.title;

      // Log nilai ID sebelum melakukan permintaan
      console.log('ID buku yang akan diambil subjudul:', ebook.id);
  
      // Ambil daftar subjudul dari backend berdasarkan ID buku
      axios.get(`http://192.168.100.7:3001/api/getSubjudul/${ebook.id}`)
        .then(response => {
          setSubjudulList(response.data); // Simpan daftar subjudul ke dalam state
          console.log('Respons daftar subjudul:', response.data); // Tambahkan ini untuk melihat respons dari backend
  
          // Jika ada data subjudul dari respons backend, atur selectedSubjudul ke subjudul pertama dalam daftar
          if (response.data && response.data.length > 0) {
            setSelectedSubjudul(response.data[0]); // Misalnya, atur selectedSubjudul ke subjudul pertama dalam daftar
          }
        })
        .catch(error => {
          console.log(error);
        });
    }
  };
  
  const handleTambahJudulPress = () => {
    setNewbookModalVisible(true);
  };

  const closeJudulModal = () => {
    setNewbookModalVisible(false);
    setRenameModalVisible(false);
  };

  const closeSubjudulModal = () => {
    setRenameSubjudulModalVisible(false);
  };
  

  const handleTambahPress = () => {
    setModalVisible(true);
  };

  const closeModal = () => {
    setModalVisible(false);
  };

  const handleClearSearch = () => {
    setSearchText('');
  };

  useEffect(() => {
    const getLastViewedTitles = async () => {
      try {
        const value = await AsyncStorage.getItem(`clickedTitles_${userId}`);
        if (value !== null) {
          setClickedTitles(JSON.parse(value));
        }
      } catch (error) {
        console.error('Error retrieving clicked titles: ', error);
      }
    };

    getLastViewedTitles();
  }, [userId]);

  
  // ambil dokumen
  const pickDocument = async () => {
    try {
      const result = await DocumentPicker.pick({
        presentationStyle: 'fullScreen',
        type: [DocumentPicker.types.pdf],
      });
      setFileResponse(result);
    } catch (err) {
      if (DocumentPicker.isCancel(err)) {
        // User canceled document selection
      } else {
        throw err;
      }
    }
  };

  const showAlert = (bab, message) => {
    Alert.alert(
      bab,
      message,
      [{ text: 'OK', onPress: () => console.log('OK Pressed') }],
      { cancelable: false }
    );
  };

  // masukkan subjudul
  const handlePost = () => {
    setLoading(true);
    setError('');
  
    const uploadedPdfName = fileResponse.length > 0 ? fileResponse[0].name : '';
  
    const formData = new FormData();
    formData.append('subjudul', subjudul); // Tambahkan subjudul ke formData
    formData.append('pdf', {
      uri: fileResponse[0].uri,
      type: 'application/pdf',
      name: uploadedPdfName,
    });
  
    // Menampilkan ID yang dikirim sebelum permintaan
    console.log('ID buku yang dikirim:', selectedEbook.id);
  
    axios.post(`http://192.168.100.7:3001/api/subjudul/${selectedEbook.id}`, formData, {
      timeout: 5000,
      headers: {
        'Content-Type': 'multipart/form-data',
      },
    })
      .then((response) => {
        console.log('Data posted successfully:', response.data);
        showAlert('Upload Berhasil', 'File PDF berhasil diunggah.');
        setTitle('');
        setFileResponse([]);
      })
      .catch((err) => {
        console.error('Error posting data:', err);
        if (err.response) {
          console.error('Response data:', err.response.data);
        }
        setError('Gagal menyimpan data. Silakan coba lagi.');
      })
      .finally(() => {
        setLoading(false);
      });
  };
  
  const clearAllUserData = async () => {
    try {
      const keys = await AsyncStorage.getAllKeys();
  
      const clickedTitlesUserIds = keys
        .filter(key => key.startsWith('clickedTitles_'))
        .map(key => key.split('_')[1]);
  
      const favoriteTitlesUserIds = keys
        .filter(key => key.startsWith('favoriteTitles_'))
        .map(key => key.split('_')[1]);
  
      const downloadedBooksUserIds = keys
        .filter(key => key.startsWith('downloadedBooks_'))
        .map(key => key.split('_')[1]);
  
      const userIds = Array.from(new Set([...clickedTitlesUserIds, ...favoriteTitlesUserIds, ...downloadedBooksUserIds]));
  
      await Promise.all(
        userIds.map(async userId => {
          // Remove clicked titles for each user
          await AsyncStorage.removeItem(`clickedTitles_${userId}`);
          console.log(`Clicked titles cleared for user ${userId}`);
  
          // Remove favorite titles for each user
          await AsyncStorage.removeItem(`favoriteTitles_${userId}`);
          console.log(`Favorite titles cleared for user ${userId}`);
  
          // Remove downloaded books for each user
          await AsyncStorage.removeItem(`downloadedBooks_${userId}`);
          console.log(`Downloaded books cleared for user ${userId}`);
        })
      );
  
      console.log('All user data cleared');
    } catch (error) {
      console.error('Error clearing user data:', error);
    }
  };
  
  
  // opsi ubah nama dan hapus judul buku
  const handleOptionPress = (selectedEbook, index) => {
    if (!selectedEbook) return;
  
    const { title, id } = selectedEbook;
    console.log('ID buku yang dipilih:', id);
    console.log('Judul buku yang dipilih:', title);
  
    Alert.alert(
      'Pilihan',
      'Pilih aksi yang ingin dilakukan:',
      [
        {
          text: 'Hapus',
          onPress: () => handleDeletePress(selectedEbook),
          style: 'destructive',
        },
        {
          text: 'Ubah Nama',
          onPress: () => handleRenamePress(selectedEbook),
        },
        {
          text: 'Batal',
          onPress: () => console.log('Cancel Pressed'),
          style: 'cancel',
        },
      ],
      { cancelable: true }
    );
  };

  const handleEllipsisPress = (selectedSubjudul) => {
    if (!selectedSubjudul) return;
  
    const { _id: subjudulId, title: judul } = selectedSubjudul;
    const { id: judulId } = selectedEbook; // Menyimpan ID judul dari selectedEbook

    console.log('ID subjudul:', subjudulId);
    console.log('ID judul:', judulId);
  
    Alert.alert(
      'Pilihan',
      'Pilih aksi yang ingin dilakukan:',
      [
        {
          text: 'Hapus',
          onPress: () => handleDeleteSubjudul(selectedSubjudul),
          style: 'destructive',
        },
        // Tambahkan aksi lain sesuai kebutuhan, misalnya, untuk mengubah subjudul
        {
          text: 'Ubah Subjudul',
          onPress: () => handleRenameSubjudulPress(selectedSubjudul),
        },
        {
          text: 'Batal',
          onPress: () => console.log('Cancel Pressed'),
          style: 'cancel',
        },
      ],
      { cancelable: true }
    );
  };
  
  
  
  // const handleDeletePress = async (selectedEbook) => {
  //   if (selectedEbook) {
  //     console.log('ID buku yang akan dihapus:', selectedEbook.id);
  
  //     try {
  //       const keys = await AsyncStorage.getAllKeys();
  
  //       const asyncStoragePromises = keys.map(async key => {
  //         const userData = await AsyncStorage.getItem(key);
  //         const userId = key.split('_')[1];
  
  //         if (userData) {
  //           let data = JSON.parse(userData) || [];
  
  //           if (Array.isArray(data)) {
  //             data = data.map(item => {
  //               if (Array.isArray(item)) {
  //                 return item.filter(title => title !== selectedEbook.title);
  //               } else if (item && item.text && Array.isArray(item.text)) {
  //                 return { ...item, text: item.text.filter(text => !text.includes(selectedEbook.title)) };
  //               }
  //               return item;
  //             });
  //           } else if (typeof data === 'string') {
  //             data = data.split(',').filter(title => title !== selectedEbook.title).join(',');
  //           }
  
  //           await AsyncStorage.setItem(`${key}_${userId}`, JSON.stringify(data));
  //           const updatedData = await AsyncStorage.getItem(`${key}_${userId}`);
  //           console.log(`Updated ${key}_${userId}:`, updatedData);
  //         }
  //       });

  //       const clickedTitlesKeys = keys.filter(key => key.startsWith('clickedTitles_'));
  
  //       await Promise.all(
  //         clickedTitlesKeys.map(async clickedTitlesKey => {
  //           const clickedTitles = await AsyncStorage.getItem(clickedTitlesKey);
  //           const userId = clickedTitlesKey.split('_')[1];
  //           let parsedClickedTitles = JSON.parse(clickedTitles) || [];
  
  //           if (Array.isArray(parsedClickedTitles)) {
  //             const titlesToDelete = selectedEbook.title.split(' - ')[0];
  //             console.log('Titles to Delete:', titlesToDelete);
  //             console.log('Parsed Clicked Titles:', parsedClickedTitles);
  
  //             const updatedClickedTitles = parsedClickedTitles.filter(title => {
  //               return !title.startsWith(titlesToDelete);
  //             });
  
  //             await AsyncStorage.setItem(`clickedTitles_${userId}`, JSON.stringify(updatedClickedTitles));
  //             console.log(`Updated clickedTitles_${userId}:`, updatedClickedTitles);
  //           }
  //         })
  //       );
      
  //       // // Hapus dari downloadedBooks
  //       // const downloadedBooksKeys = keys.filter(key => key.startsWith('downloadedBooks_'));
  //       // await Promise.all(
  //       //   downloadedBooksKeys.map(async downloadedBooksKey => {
  //       //     const downloadedBooks = await AsyncStorage.getItem(downloadedBooksKey);
  //       //     const userId = downloadedBooksKey.split('_')[1];
  //       //     const parsedDownloadedBooks = JSON.parse(downloadedBooks) || [];
  //       //     const filteredDownloadedBooks = parsedDownloadedBooks.filter(book => !book.text.includes(selectedEbook.title));
  //       //     await AsyncStorage.setItem(`downloadedBooks_${userId}`, JSON.stringify(filteredDownloadedBooks));
  //       //     const updatedDownloadedBooks = await AsyncStorage.getItem(`downloadedBooks_${userId}`);
  //       //     console.log(`Updated downloadedBooks_${userId}:`, updatedDownloadedBooks);
  //       //   })
  //       // );
  
  //       const response = await axios.delete(`http://192.168.100.7:3001/api/buku/${selectedEbook.id}`);
  //       console.log('Book deleted:', response.data);
  //       // ... (remaining part remains the same)
  //     } catch (error) {
  //       console.error('Error:', error);
  //     }
  //   }
  // };
  
  // const handleDeletePress = async (selectedEbook) => {
  //   if (selectedEbook) {
  //     console.log('ID buku yang akan dihapus:', selectedEbook.id);
  
  //     try {
  //       const keys = await AsyncStorage.getAllKeys();
  
  //       // Mengumpulkan seluruh userId dari clickedTitles dan favoriteTitles
  //       const clickedTitlesUserIds = keys
  //         .filter(key => key.startsWith('clickedTitles_'))
  //         .map(key => key.split('_')[1]);
  
  //       const favoriteTitlesUserIds = keys
  //         .filter(key => key.startsWith('favoriteTitles_'))
  //         .map(key => key.split('_')[1]);
  
  //       const userIds = Array.from(new Set([...clickedTitlesUserIds, ...favoriteTitlesUserIds]));
  
  //       // Proses untuk setiap userId
  //       await Promise.all(
  //         userIds.map(async userId => {
  //           // Update clickedTitles
  //           const clickedTitlesKey = `clickedTitles_${userId}`;
  //           const clickedTitles = await AsyncStorage.getItem(clickedTitlesKey);
  //           let parsedClickedTitles = JSON.parse(clickedTitles) || [];
  
  //           if (Array.isArray(parsedClickedTitles)) {
  //             const titlesToDelete = selectedEbook.title.split(' - ')[0];
  //             const updatedClickedTitles = parsedClickedTitles.filter(title => {
  //               return !title.startsWith(titlesToDelete);
  //             });
  
  //             await AsyncStorage.setItem(clickedTitlesKey, JSON.stringify(updatedClickedTitles));
  //             console.log(`Updated clickedTitles_${userId}:`, updatedClickedTitles);
  //           }
  
  //           // Update favoriteTitles
  //           const favoriteTitlesKey = `favoriteTitles_${userId}`;
  //           const favoriteTitles = await AsyncStorage.getItem(favoriteTitlesKey);
  //           let parsedFavoriteTitles = JSON.parse(favoriteTitles) || [];
  
  //           if (Array.isArray(parsedFavoriteTitles)) {
  //             const updatedFavoriteTitles = parsedFavoriteTitles.filter(title => {
  //               return !title.startsWith(titlesToDelete);
  //             });
  
  //             await AsyncStorage.setItem(favoriteTitlesKey, JSON.stringify(updatedFavoriteTitles));
  //             console.log(`Updated favoriteTitles_${userId}:`, updatedFavoriteTitles);
  //           }
  //         })
  //       );
  
  //       // Hapus buku dari server
  //       const response = await axios.delete(`http://192.168.100.7:3001/api/buku/${selectedEbook.id}`);
  //       console.log('Book deleted:', response.data);
  //       // ... (remaining part remains the same)
  //     } catch (error) {
  //       console.error('Error:', error);
  //     }
  //   }
  // };
  
  // const handleDeletePress = async (selectedEbook) => {
  //   if (selectedEbook) {
  //     console.log('ID buku yang akan dihapus:', selectedEbook.id);
  
  //     try {
  //       const keys = await AsyncStorage.getAllKeys();
  
  //       // Mengumpulkan seluruh userId dari clickedTitles dan favoriteTitles
  //       const clickedTitlesUserIds = keys
  //         .filter(key => key.startsWith('clickedTitles_'))
  //         .map(key => key.split('_')[1]);
  
  //       const favoriteTitlesUserIds = keys
  //         .filter(key => key.startsWith('favoriteTitles_'))
  //         .map(key => key.split('_')[1]);
  
  //       const userIds = Array.from(new Set([...clickedTitlesUserIds, ...favoriteTitlesUserIds]));
  
  //       // Proses untuk setiap userId
  //       await Promise.all(
  //         userIds.map(async userId => {
  //           // Update clickedTitles
  //           const clickedTitlesKey = `clickedTitles_${userId}`;
  //           const clickedTitles = await AsyncStorage.getItem(clickedTitlesKey);
  //           let parsedClickedTitles = JSON.parse(clickedTitles) || [];
  
  //           if (Array.isArray(parsedClickedTitles)) {
  //             const titlesToDelete = selectedEbook.title.split(' - ')[0];
  //             const updatedClickedTitles = parsedClickedTitles.filter(title => {
  //               return !title.startsWith(titlesToDelete);
  //             });
  
  //             await AsyncStorage.setItem(clickedTitlesKey, JSON.stringify(updatedClickedTitles));
  //             console.log(`Updated clickedTitles_${userId}:`, updatedClickedTitles);
  //           }
  
  //           // Update favoriteTitles
  //           const favoriteTitlesKey = `favoriteTitles_${userId}`;
  //           const favoriteTitles = await AsyncStorage.getItem(favoriteTitlesKey);
  //           let parsedFavoriteTitles = JSON.parse(favoriteTitles) || [];
  
  //           if (Array.isArray(parsedFavoriteTitles)) {
  //             const titlesToDelete = selectedEbook.title.split(' - ')[0];
  //             const updatedFavoriteTitles = parsedFavoriteTitles.filter(title => {
  //               return !title.startsWith(titlesToDelete);
  //             });
  
  //             await AsyncStorage.setItem(favoriteTitlesKey, JSON.stringify(updatedFavoriteTitles));
  //             console.log(`Updated favoriteTitles_${userId}:`, updatedFavoriteTitles);
  //           }
  //         })
  //       );
  
  //       // Hapus buku dari server
  //       const response = await axios.delete(`http://192.168.100.7:3001/api/buku/${selectedEbook.id}`);
  //       console.log('Book deleted:', response.data);
  //       // ... (remaining part remains the same)
  //     } catch (error) {
  //       console.error('Error:', error);
  //     }
  //   }
  // };
  
  
  // const handleDeletePress = (selectedEbook) => {
  //   if (selectedEbook) {
  //     // Menampilkan ID yang dipilih sebelum penghapusan
  //     console.log('ID buku yang akan dihapus:', selectedEbook.id);
  
  //     // Mengambil data dari AsyncStorage
  //     AsyncStorage.multiGet([`clickedTitles_${userId}`, `favoriteTitles_${userId}`, `downloadedBooks_${userId}`])
  //       .then(data => {
  //         const [clickedTitles, favoriteTitles, downloadedBooks] = data.map(item => JSON.parse(item[1]) || []);
  
          // // Filter clickedTitles
          // const filteredClickedTitles = clickedTitles.filter(title => {
          //   // Sesuaikan logika berikut sesuai kebutuhan
          //   return !title.includes(selectedEbook.title);
          // });
  
          // // Filter favoriteTitles
          // const filteredFavoriteTitles = favoriteTitles.filter(title => {
          //   // Sesuaikan logika berikut sesuai kebutuhan
          //   return !title.includes(selectedEbook.title);
          // });

          // // Filter downloadedBooks
          // const filteredDownloadedBooks = downloadedBooks.filter(book => {
          //   return !book.text.includes(selectedEbook.title);
          // });


  
  //         // Simpan kembali ke AsyncStorage
  //         AsyncStorage.multiSet([
  //           [`clickedTitles_${userId}`, JSON.stringify(filteredClickedTitles)],
  //           [`favoriteTitles_${userId}`, JSON.stringify(filteredFavoriteTitles)],
  //           [`downloadedBooks_${userId}`, JSON.stringify(filteredDownloadedBooks)]
  //         ])
  //           .then(() => {
  //             // Lanjutkan dengan penghapusan buku
  //             axios.delete(`http://192.168.100.7:3001/api/buku/${selectedEbook.id}`)
  //               .then(response => {
  //                 console.log('Book deleted:', response.data);
  //                 // ... (bagian lainnya tetap sama)
  //               })
  //               .catch(error => {
  //                 console.error('Error deleting book:', error);
  //                 if (error.response) {
  //                   console.error('Server response:', error.response.data);
  //                 }
  //               });
  //           })
  //           .catch(error => {
  //             console.error('Error updating AsyncStorage:', error);
  //           });
  //       })
  //       .catch(error => {
  //         console.error('Error retrieving data from AsyncStorage:', error);
  //       });
  //   }
  // }
  
  
  // const handleDeletePress = async (selectedEbook) => {
  //   if (selectedEbook) {
  //     console.log('ID buku yang akan dihapus:', selectedEbook.id);
  
  //     try {
  //       const keys = await AsyncStorage.getAllKeys();
  
  //       const clickedTitlesUserIds = keys
  //         .filter(key => key.startsWith('clickedTitles_'))
  //         .map(key => key.split('_')[1]);
  
  //       const favoriteTitlesUserIds = keys
  //         .filter(key => key.startsWith('favoriteTitles_'))
  //         .map(key => key.split('_')[1]);
  
  //       const downloadedBooksUserIds = keys
  //         .filter(key => key.startsWith('downloadedBooks_'))
  //         .map(key => key.split('_')[1]);
  
  //       const userIds = Array.from(new Set([...clickedTitlesUserIds, ...favoriteTitlesUserIds, ...downloadedBooksUserIds]));
  
  //       await Promise.all(
  //         userIds.map(async userId => {
  //           // Update clickedTitles
  //           const clickedTitlesKey = `clickedTitles_${userId}`;
  //           const clickedTitles = await AsyncStorage.getItem(clickedTitlesKey);
  //           let parsedClickedTitles = JSON.parse(clickedTitles) || [];
  
  //           if (Array.isArray(parsedClickedTitles)) {
  //             const titlesToDelete = selectedEbook.title.split(' - ')[0];
  //             const updatedClickedTitles = parsedClickedTitles.filter(title => !title.startsWith(titlesToDelete));
  //             await AsyncStorage.setItem(clickedTitlesKey, JSON.stringify(updatedClickedTitles));
  //             console.log(`Updated clickedTitles_${userId}:`, updatedClickedTitles);
  //           }
  
  //           // Update favoriteTitles
  //           const favoriteTitlesKey = `favoriteTitles_${userId}`;
  //           const favoriteTitles = await AsyncStorage.getItem(favoriteTitlesKey);
  //           let parsedFavoriteTitles = JSON.parse(favoriteTitles) || [];
  
  //           if (Array.isArray(parsedFavoriteTitles)) {
  //             const titlesToDelete = selectedEbook.title.split(' - ')[0];
  //             const updatedFavoriteTitles = parsedFavoriteTitles.filter(title => !title.startsWith(titlesToDelete));
  //             await AsyncStorage.setItem(favoriteTitlesKey, JSON.stringify(updatedFavoriteTitles));
  //             console.log(`Updated favoriteTitles_${userId}:`, updatedFavoriteTitles);
  //           }
  
  //           // Update downloadedBooks
  //           const downloadedBooksKey = `downloadedBooks_${userId}`;
  //           const downloadedBooks = await AsyncStorage.getItem(downloadedBooksKey);
  //           let parsedDownloadedBooks = JSON.parse(downloadedBooks) || [];
  
  //           if (Array.isArray(parsedDownloadedBooks)) {
  //             const filteredDownloadedBooks = parsedDownloadedBooks.filter(book => !book.text.includes(selectedEbook.title));
  //             await AsyncStorage.setItem(downloadedBooksKey, JSON.stringify(filteredDownloadedBooks));
  //             console.log(`Updated downloadedBooks_${userId}:`, filteredDownloadedBooks);
  //           }
  //         })
  //       );
  
  //       const response = await axios.delete(`http://192.168.100.7:3001/api/buku/${selectedEbook.id}`);
  //       console.log('Book deleted:', response.data);
  //       // ... (remaining part remains the same)
  //     } catch (error) {
  //       console.error('Error:', error);
  //     }
  //   }
  // };

  // menghapus judul
  const handleDeletePress = async (selectedEbook) => {
    if (selectedEbook) {
      console.log('ID buku yang akan dihapus:', selectedEbook.id);
  
      try {
        const keys = await AsyncStorage.getAllKeys();
  
        const clickedTitlesUserIds = keys
          .filter(key => key.startsWith('clickedTitles_'))
          .map(key => key.split('_')[1]);
  
        const favoriteTitlesUserIds = keys
          .filter(key => key.startsWith('favoriteTitles_'))
          .map(key => key.split('_')[1]);
  
        const downloadedBooksUserIds = keys
          .filter(key => key.startsWith('downloadedBooks_'))
          .map(key => key.split('_')[1]);
  
        const userIds = Array.from(new Set([...clickedTitlesUserIds, ...favoriteTitlesUserIds, ...downloadedBooksUserIds]));
  
        await Promise.all(
          userIds.map(async userId => {
            // Update clickedTitles
            const clickedTitlesKey = `clickedTitles_${userId}`;
            const clickedTitles = await AsyncStorage.getItem(clickedTitlesKey);
            let parsedClickedTitles = JSON.parse(clickedTitles) || [];
  
            if (Array.isArray(parsedClickedTitles)) {
              const titlesToDelete = selectedEbook.title.split(' - ')[0];
              const updatedClickedTitles = parsedClickedTitles.filter(title => !title.startsWith(titlesToDelete));
              await AsyncStorage.setItem(clickedTitlesKey, JSON.stringify(updatedClickedTitles));
              console.log(`Updated clickedTitles_${userId}:`, updatedClickedTitles);
            }
  
            // Update favoriteTitles
            const favoriteTitlesKey = `favoriteTitles_${userId}`;
            const favoriteTitles = await AsyncStorage.getItem(favoriteTitlesKey);
            let parsedFavoriteTitles = JSON.parse(favoriteTitles) || [];
  
            if (Array.isArray(parsedFavoriteTitles)) {
              const titlesToDelete = selectedEbook.title.split(' - ')[0];
              const updatedFavoriteTitles = parsedFavoriteTitles.filter(title => !title.startsWith(titlesToDelete));
              await AsyncStorage.setItem(favoriteTitlesKey, JSON.stringify(updatedFavoriteTitles));
              console.log(`Updated favoriteTitles_${userId}:`, updatedFavoriteTitles);
            }
  
            // Update downloadedBooks
            const downloadedBooksKey = `downloadedBooks_${userId}`;
            const downloadedBooks = await AsyncStorage.getItem(downloadedBooksKey);
            let parsedDownloadedBooks = JSON.parse(downloadedBooks) || [];
  
            if (Array.isArray(parsedDownloadedBooks)) {
              const filteredDownloadedBooks = parsedDownloadedBooks.filter(book => !book.text.includes(selectedEbook.title));
              await AsyncStorage.setItem(downloadedBooksKey, JSON.stringify(filteredDownloadedBooks));
              console.log(`Updated downloadedBooks_${userId}:`, filteredDownloadedBooks);
            }
          })
        );
  
        const response = await axios.delete(`http://192.168.100.7:3001/api/buku/${selectedEbook.id}`);
        console.log('Book deleted:', response.data);
        // ... (remaining part remains the same)
      } catch (error) {
        console.error('Error:', error);
      }
    }
  };
  
  // hapus subjudul

  const handleDeleteSubjudul = async (selectedSubjudul) => {
    if (selectedSubjudul) {
      console.log('ID subjudul yang akan dihapus:', selectedSubjudul._id);
  
      try {
        const keys = await AsyncStorage.getAllKeys();
  
        const clickedTitlesUserIds = keys
          .filter(key => key.startsWith('clickedTitles_'))
          .map(key => key.split('_')[1]);
  
        const favoriteTitlesUserIds = keys
          .filter(key => key.startsWith('favoriteTitles_'))
          .map(key => key.split('_')[1]);
  
        const downloadedBooksUserIds = keys
          .filter(key => key.startsWith('downloadedBooks_'))
          .map(key => key.split('_')[1]);
  
        const userIds = Array.from(new Set([...clickedTitlesUserIds, ...favoriteTitlesUserIds, ...downloadedBooksUserIds]));
  
        await Promise.all(
          userIds.map(async userId => {
            // Update clickedTitles
            const clickedTitlesKey = `clickedTitles_${userId}`;
            const clickedTitles = await AsyncStorage.getItem(clickedTitlesKey);
            let parsedClickedTitles = JSON.parse(clickedTitles) || [];
  
            if (Array.isArray(parsedClickedTitles)) {
              const subjudulToDelete = selectedSubjudul.subjudul.split(' - ')[0];
              const updatedClickedTitles = parsedClickedTitles.filter(title => !title.includes(subjudulToDelete));
              await AsyncStorage.setItem(clickedTitlesKey, JSON.stringify(updatedClickedTitles));
              console.log(`Updated clickedTitles_${userId}:`, updatedClickedTitles);
            }
  
            // Update favoriteTitles
            const favoriteTitlesKey = `favoriteTitles_${userId}`;
            const favoriteTitles = await AsyncStorage.getItem(favoriteTitlesKey);
            let parsedFavoriteTitles = JSON.parse(favoriteTitles) || [];
  
            if (Array.isArray(parsedFavoriteTitles)) {
              const subjudulToDelete = selectedSubjudul.subjudul.split(' - ')[0];
              const updatedFavoriteTitles = parsedFavoriteTitles.filter(title => !title.includes(subjudulToDelete));
              await AsyncStorage.setItem(favoriteTitlesKey, JSON.stringify(updatedFavoriteTitles));
              console.log(`Updated favoriteTitles_${userId}:`, updatedFavoriteTitles);
            }
  
            // Update downloadedBooks
            const downloadedBooksKey = `downloadedBooks_${userId}`;
            const downloadedBooks = await AsyncStorage.getItem(downloadedBooksKey);
            let parsedDownloadedBooks = JSON.parse(downloadedBooks) || [];
  
            if (Array.isArray(parsedDownloadedBooks)) {
              const filteredDownloadedBooks = parsedDownloadedBooks.filter(book => !book.text.includes(selectedSubjudul.subjudul));
              await AsyncStorage.setItem(downloadedBooksKey, JSON.stringify(filteredDownloadedBooks));
              console.log(`Updated downloadedBooks_${userId}:`, filteredDownloadedBooks);
            }
          })
        );
  
        const response = await axios.delete(`http://192.168.100.7:3001/api/books/${selectedEbook.id}/subjudul/${selectedSubjudul._id}`);
        console.log('Book deleted:', response.data);
        // ... (remaining part remains the same)
      } catch (error) {
        console.error('Error:', error);
      }
    }
  };
  
  
  

// rename judul buku
const handleRenameBook = (selectedEbook) => {
  if (selectedEbook) {
    console.log('New Book Name:', newBookName);
    console.log('Current favorite titles:', favoriteTitles);
    console.log('Selected eBook Title:', selectedEbook.title);
    
    axios.put(`http://192.168.100.7:3001/api/updateBuku/${selectedEbook.id}`, { newTitle: newBookName })
      .then(response => {
        console.log('Book updated:', response.data);

        // Clear AsyncStorage for downloadedBooks, favoriteTitles, clickedTitles for all userIds
        AsyncStorage.getAllKeys()
          .then(keys => {
            const userIdKeys = keys.filter(key => key.startsWith('downloadedBooks_') || key.startsWith('favoriteTitles_') || key.startsWith('clickedTitles_'));
            
            AsyncStorage.multiRemove(userIdKeys)
              .then(() => {
                console.log('AsyncStorage cleared for downloaded books, favorite titles, clicked titles for all userIds');
                setDownloadedBooks([]); 
                setFavoriteTitles([]); 
                setClickedTitles([]); 
                setRenameModalVisible(false); 
              })
              .catch((error) => {
                console.error('Error clearing AsyncStorage:', error);
              });
          })
          .catch(error => {
            console.error('Error getting AsyncStorage keys:', error);
          });
      })
      .catch(error => {
        console.error('Error updating book:', error);
      });
  }
};

//rename subjudul
const handleRenameSubjudul = (selectedSubjudul) => {
  if (selectedSubjudul) {
    console.log('New Subjudul Title:', newSubjudulName);
    
    axios.put(`http://192.168.100.7:3001/api/updateBuku/${selectedEbook.id}/subjudul/${selectedSubjudul._id}`, { updatedSubjudul: newSubjudulName })
      .then(response => {
        console.log('Subjudul updated:', response.data);

        // Clear AsyncStorage for downloadedBooks, favoriteTitles, clickedTitles for all userIds
        AsyncStorage.getAllKeys()
          .then(keys => {
            const userIdKeys = keys.filter(key => key.startsWith('downloadedBooks_') || key.startsWith('favoriteTitles_') || key.startsWith('clickedTitles_'));
            
            AsyncStorage.multiRemove(userIdKeys)
              .then(() => {
                console.log('AsyncStorage cleared for downloaded books, favorite titles, clicked titles for all userIds');
                setDownloadedBooks([]); 
                setFavoriteTitles([]); 
                setClickedTitles([]); 
                setRenameSubjudulModalVisible(false); 
              })
              .catch((error) => {
                console.error('Error clearing AsyncStorage:', error);
              });
          })
          .catch(error => {
            console.error('Error getting AsyncStorage keys:', error);
          });
      })
      .catch(error => {
        console.error('Error updating subjudul:', error);
      });
  }
};


  
  
  const removeTitleFromAsyncStorage = async (titleToRemove) => {
    try {
      const updatedTitles = clickedTitles.filter(title => title !== titleToRemove);
      await AsyncStorage.setItem(`clickedTitles_${userId}`, JSON.stringify(updatedTitles));
      setClickedTitles(updatedTitles);
      console.log('Title removed from AsyncStorage:', titleToRemove);
    } catch (error) {
      console.error('Error removing title from AsyncStorage:', error);
    }
  };
  
  const updateBookListAndTitles = (deletedBookTitle) => {
    // Update the book list after deletion
    axios.get('http://192.168.100.7:3001/api/pdf')
      .then(response => {
        setBukuList(response.data);
  
        // Update clickedTitles to remove the deleted book's title
        const updatedClickedTitles = clickedTitles.filter(title => title !== deletedBookTitle);
        setClickedTitles(updatedClickedTitles);
      })
      .catch(error => {
        console.log(error);
      });
  };

  useEffect(() => {
    const fetchDownloadedBooks = async () => {
      try {
        const storedBooks = await AsyncStorage.getItem(`downloadedBooks_${userId}`);
        if (storedBooks) {
          setDownloadedBooks(JSON.parse(storedBooks));
        }
      } catch (error) {
        console.error('Error fetching downloaded books:', error);
      }
    };

    fetchDownloadedBooks();
  }, []);
  
  const handleRenamePress = (selectedEbook) => {
    if (!selectedEbook) return;
  
    // Set selected book for rename
    setSelectedBookForRename(selectedEbook);
    // Set initial new book name as the selected book's title
    setNewBookName(selectedEbook.title);
    // Show rename modal
    setRenameModalVisible(true);
  };

  const handleRenameSubjudulPress = (selectedSubjudul) => {
    if (!selectedSubjudul) return;
  
    // Set selected book for rename
    setSelectedSubjudulForRename(selectedSubjudul);
    // Set initial new book name as the selected book's title
    setNewSubjudulName(selectedSubjudul.subjudul);
    // Show rename modal
    setRenameSubjudulModalVisible(true);
  };
  
  
  useEffect(() => {
    // Ambil daftar judul favorit dari AsyncStorage saat komponen dimuat
    const fetchData = async () => {
      try {
        const storedTitles = await AsyncStorage.getItem(`favoriteTitles_${userId}`);
        if (storedTitles) {
          setFavoriteTitles(JSON.parse(storedTitles));
        }
      } catch (error) {
        // Handle error if needed
        console.error(error);
      }
    };

    fetchData();
  }, []);

  

  // Navigasi ke stiap subjudul
  // Navigasi ke stiap subjudul
  const handleSubjudulPress = (selectedSubjudul) => {
    if (selectedSubjudul) {
      const { subjudul, title, name, _id, path } = selectedSubjudul;
      const selectedTitle = selectedEbook ? selectedEbook.title : '';
      const clickedTitleEntry = `${selectedTitle} - ${subjudul}`;
      // Log ID yang dikirim
      console.log('Selected Subjudul ID:', _id);
  
      // Filter entri yang tidak sama dengan entri yang baru
      const updatedClickedTitles = clickedTitles.filter((clickedTitle) => clickedTitle !== clickedTitleEntry);
  
      // Tambahkan entri baru ke depan array
      updatedClickedTitles.unshift(clickedTitleEntry);
  
      setClickedTitles(updatedClickedTitles);
  
      // Simpan ke AsyncStorage
      AsyncStorage.setItem(`clickedTitles_${userId}`, JSON.stringify(updatedClickedTitles))
        .then(() => {
          console.log('Clicked titles saved:', updatedClickedTitles);
        })
        .catch((error) => {
          console.error('Error saving clicked titles:', error);
        });
  
      // Navigasi ke layar PDFViewer dengan judul buku dan subjudul
      navigation.navigate('PDFViewer_Daftar', { title: selectedTitle, subjudul, name, id: _id, path, clickedEntry: `${selectedTitle} - ${subjudul}`, clickedTitleEntry });
    }
  };
  

  // Buat judul baru
  const handleNewbook = () => {
    // Pastikan judul buku yang baru dimasukkan ke state sudah sesuai
    setLoading(true);
    setError('');
  
    axios.post('http://192.168.100.7:3001/api/createbook', { title: judul })
      .then((response) => {
        console.log('New book added successfully:', response.data);
        setJudul('');
        setNewbookModalVisible(false);
        // Lakukan apapun yang diperlukan setelah buku ditambahkan
      })
      .catch((err) => {
        console.error('Error adding new book:', err);
        if (err.response) {
          console.error('Response data:', err.response.data);
        }
        setError('Gagal menambahkan buku. Silakan coba lagi.');
      })
      .finally(() => {
        setLoading(false);
      });
  };
  
  
  
  
  
  

  return (
    <TouchableWithoutFeedback onPress={Keyboard.dismiss} accessible={false}>
      <View style={{ flex: 1, backgroundColor: '#ffffff' }}>
        {/* Header Section */}
        <View style={styles.header}>
          <StatusBar barStyle={'dark-content'} backgroundColor={'#f8f8ff'} />
          <ImageBackground
            source={require('../../asset/images/bgdaftar.jpeg')}
            style={{ width: '100%', height: '100%', resizeMode: 'cover', borderBottomLeftRadius: 20, borderBottomRightRadius: 20, overflow: 'hidden' }}
          >
            <View style={styles.searchContainer}>
              <Icon name="search" size={20} color="#878789" style={styles.searchIcon} />
              <TextInput
                style={styles.textInput}
                placeholder='Cari Buku'
                value={searchText}
                onChangeText={text => setSearchText(text)}
              />
              {searchText.length > 0 && (
                <TouchableOpacity onPress={handleClearSearch} style={styles.clearButton}>
                  <Icon name="close" size={20} color="grey" />
                </TouchableOpacity>
              )}
            </View>
          </ImageBackground>
        </View>

        {/* Content Section */}
        <ScrollView style={{ flex: 1 }}>
          <View style={styles.content}>
            <View style={{
              marginHorizontal: 20,
              flexDirection: 'row',
              marginTop: 10,
            }}>
              <StatusBar barStyle={'dark-content'} backgroundColor={'#f8f8ff'} />
              <Text style={{ fontSize: 25, color: 'black', paddingLeft: 0, fontWeight: 'bold' }}>Daftar Buku</Text>
            </View>

            {filteredEbooks.map((ebook, index) => (
              <View key={index}>
                <TouchableOpacity onPress={() => handleEbookPress(ebook, index)}>
                  <View
                    style={{
                      marginHorizontal: 20,
                      flexDirection: 'row',
                      justifyContent: 'space-between',
                      alignItems: 'center',
                      marginTop: 10,
                    }}>
                    <Text style={{ fontSize: 20, color: 'black', paddingLeft: 0 }}>{ebook.title}</Text>
                    <View style={{ flexDirection: 'row', alignItems: 'center', marginRight: 10 }}>
                      <TouchableOpacity onPress={handleTambahPress}>
                        <Icon name="add-circle-sharp" size={25} color="maroon" />
                      </TouchableOpacity>

                      <TouchableOpacity onPress={() => handleOptionPress(ebook, index)}>
                        <Icon name="ellipsis-vertical" size={25} color="maroon" />
                      </TouchableOpacity>
                      <View style={{ flexDirection: 'row', marginLeft: 10 }}>
                        <Icon
                          name={selectedEbookIndex === index && showButtons ? "arrow-down" : "arrow-forward"}
                          size={20}
                          color="black"
                        />
                      </View>
                    </View>
                  </View>
                </TouchableOpacity>

                {/* {selectedEbook && selectedEbook.title === ebook.title && showButtons && (
                  <View style={styles.buttonsContainer}>
                    <TouchableOpacity style={styles.button} onPress={handleBuku}>
                      <Text style={styles.buttonText}>View</Text>
                    </TouchableOpacity>
                  </View>
                )} */}

                {/* {selectedEbook && selectedEbook.title === ebook.title && showButtons && subjudulList && (
                  <ScrollView>
                    {subjudulList.map((item, index) => (
                      <TouchableOpacity
                        key={index.toString()}
                        style={styles.button}
                        onPress={() => handleSubjudulPress(item)}
                      >
                        <Text style={styles.buttonText}>{item.subjudul}</Text>
                      </TouchableOpacity>
                    ))}
                  </ScrollView>
                )} */}

                {selectedEbook && selectedEbook.title === ebook.title && showButtons && subjudulList && (
                  <ScrollView>
                    {subjudulList.map((item, index) => (
                      <TouchableOpacity
                        key={index.toString()}
                        style={styles.buttonContainer}
                        onPress={() => handleSubjudulPress(item)}
                      >
                        <View style={{ flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between', width: '100%' }}>
                          <Text numberOfLines={1} ellipsizeMode="tail" style={[styles.buttonText, { maxWidth: '90%' }]}>{item.subjudul}</Text>
                          <TouchableOpacity onPress={() => handleEllipsisPress(item)}>
                            <Icon name="ellipsis-vertical" size={20} color="maroon" />
                          </TouchableOpacity>
                        </View>
                      </TouchableOpacity>
                    ))}
                  </ScrollView>
                )}





                



                <View style={styles.divider} />
              </View>
            ))}
          </View>
        </ScrollView>

        {/* <View style={{ flex: 1, backgroundColor: '#ffffff' }}> */}

          {/* Clear Titles Button */}
          <TouchableOpacity onPress={clearAllUserData} style={styles.clearTitlesButton}>
            <Text style={{ color: 'red', fontSize: 18 }}>Clear Saved Titles</Text>
          </TouchableOpacity>

          {/* Add New Book Button */}
          <TouchableOpacity onPress={handleTambahJudulPress} style={styles.fabAddButton}>
            <Icon name="add-circle-sharp" size={50} color="white" />
          </TouchableOpacity>

        {/* </View> */}


        {/* Modal buku baru */}
        <Modal
          animationType="fade"
          transparent={true}
          visible={modalVisible}
          onRequestClose={closeModal}
        >
          <TouchableWithoutFeedback onPress={closeModal}>
            <View style={styles.modalContainer}>
              <TouchableWithoutFeedback>
                <View style={styles.modalContent}>
                  <Text style={styles.modalTitle}>Tambah Buku Baru</Text>
                  <TextInput
                    style={styles.modalInput}
                    placeholder="Nama Buku"
                    value={subjudul} // Ganti bab menjadi newBookName
                    onChangeText={text => setSubjudul(text)}
                  />


                  <StatusBar barStyle={'dark-content'} />
                  {fileResponse.map((file, index) => (
                    <Text
                      key={index.toString()}
                      style={styles.uri}
                      numberOfLines={1}
                      ellipsizeMode={'middle'}
                    >
                      {file?.name}
                    </Text>
                  ))}

                  <TouchableOpacity
                    style={styles.pdfButton}
                    onPress={pickDocument}
                  >  
                  <Text style={styles.pdfButtonText}>Pilih File PDF</Text>
                  </TouchableOpacity>

                  <TouchableOpacity
                    style={styles.modalButton}
                    onPress={handlePost}
                    disabled={loading}
                  >
                    <Text style={styles.modalButtonText}>Tambah</Text>
                  </TouchableOpacity>
                </View>
              </TouchableWithoutFeedback>
            </View>
          </TouchableWithoutFeedback>
        </Modal>

        {/* Modal Rename Judul */}

        <Modal
          animationType="fade"
          transparent={true}
          visible={renameModalVisible}
          onRequestClose={closeJudulModal}
        >
          <TouchableWithoutFeedback onPress={closeJudulModal}>
          <View style={styles.modalContainer}>
            <View style={styles.modalContent}>
              <Text style={styles.modalTitle}>Ubah Nama</Text>
              <TextInput
                style={styles.modalInput}
                placeholder="Masukkan nama baru"
                value={newBookName}
                onChangeText={text => setNewBookName(text)}
              />
              <TouchableOpacity
                style={styles.modalButton}
                onPress={() => handleRenameBook(selectedBookForRename)}
              >
                <Text style={styles.modalButtonText}>Simpan Perubahan</Text>
              </TouchableOpacity>
            </View>
          </View>
          </TouchableWithoutFeedback>
        </Modal>

        {/* Modal Rename Subjudul */}

        <Modal
          animationType="fade"
          transparent={true}
          visible={renameSubjudulModalVisible}
          onRequestClose={closeSubjudulModal}
        >
          <TouchableWithoutFeedback onPress={closeSubjudulModal}>
          <View style={styles.modalContainer}>
            <View style={styles.modalContent}>
              <Text style={styles.modalTitle}>Ubah Subjudul</Text>
              <TextInput
                style={styles.modalInput}
                placeholder="Masukkan Subjudul baru"
                value={newSubjudulName}
                onChangeText={text => setNewSubjudulName(text)}
              />
              <TouchableOpacity
                style={styles.modalButton}
                onPress={() => handleRenameSubjudul(selectedSubjudulForRename)}
              >
                <Text style={styles.modalButtonText}>Simpan Perubahan</Text>
              </TouchableOpacity>
            </View>
          </View>
          </TouchableWithoutFeedback>
        </Modal>

        {/* Modal Tambah Judul */}

        <Modal
          animationType="fade"
          transparent={true}
          visible={newbookModalVisible}
          onRequestClose={() => setNewbookModalVisible(false)}
        >
          <TouchableWithoutFeedback onPress={() => setNewbookModalVisible(false)}>
            <View style={styles.modalContainer}>
              <View style={styles.modalContent}>
                <Text style={styles.modalTitle}>Tambah Judul</Text>
                <TextInput
                  style={styles.modalInput}
                  placeholder="Masukkan Judul Baru"
                  value={judul}
                  onChangeText={text => setJudul(text)}
                />
                <TouchableOpacity
                  style={styles.modalButton}
                  onPress={handleNewbook} // Ubah menjadi handleNewbook tanpa tanda kurung
                >
                  <Text style={styles.modalButtonText}>Simpan Perubahan</Text>
                </TouchableOpacity>
              </View>
            </View>
          </TouchableWithoutFeedback>
        </Modal>



        <MenuBar />
      </View>
    </TouchableWithoutFeedback>
  );
};

const styles = StyleSheet.create({
  header: {
    width: width * 1,
    height: width * 0.5,
    borderBottomRightRadius: 20,
    borderBottomLeftRadius: 20,
  },
  searchContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    backgroundColor: '#EEEEEE',
    height: 40,
    borderRadius: 10,
    margin: 25,
    marginTop: 125,
    paddingLeft: 10,
    position: 'relative', // Added this line
  },
  searchIcon: {
    marginRight: 10,
  },
  textInput: {
    flex: 1,
    fontSize: 17,
    color: '#878789',
  },
  clearButton: {
    position: 'absolute',
    right: 10,
    top: '50%', // Centered vertically
    marginTop: -10, // Adjusted for half of the button height
  },
  content: {
    flex: 1,
  },
  buttonsContainer: {
    flexDirection: 'row',
    justifyContent: 'space-around',
    marginTop: 10,
  },
  button: {
    flex: 1,
    backgroundColor: 'red',
    paddingVertical: 10,
    borderRadius: 5,
    marginHorizontal: 10,
    marginTop: 10,
    marginBottom: 10,
  },
  buttonText: {
    color: 'white',
    fontSize: 16,
    textAlign: 'center',
  },
  divider: {
    height: StyleSheet.hairlineWidth,
    backgroundColor: '#A9ABB1',
    marginHorizontal: 16,
    marginTop: 16,
  },
  fab: {
    position: 'absolute',
    bottom: 80,
    right: 20,
    backgroundColor: 'maroon',
    borderRadius: 30,
    width: 60,
    height: 60,
    justifyContent: 'center',
    alignItems: 'center',
    elevation: 0,
  },
  modalContainer: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'rgba(0, 0, 0, 0.5)',
  },
  modalContent: {
    backgroundColor: 'white',
    padding: 20,
    borderRadius: 10,
    width: width - 40,
  },
  modalTitle: {
    fontSize: 20,
    fontWeight: 'bold',
    marginBottom: 10,
  },
  modalInput: {
    borderWidth: 1,
    borderColor: '#ccc',
    borderRadius: 5,
    padding: 10,
    marginBottom: 10,
  },
  modalButton: {
    backgroundColor: 'maroon',
    padding: 10,
    borderRadius: 5,
    alignItems: 'center',
  },
  modalButtonText: {
    color: 'white',
    fontSize: 16,
  },
  pdfButtonText: {
    color: 'white',
    fontSize: 15,
  },
  pdfButton: {
    backgroundColor: 'maroon',
    borderRadius: 5,
    padding: 10,
    marginBottom: 10,
    alignItems: 'center',
  },
  clearTitlesButton: {
    backgroundColor: 'white',
    borderWidth: 1,
    borderColor: 'red',
    padding: 10,
    borderRadius: 5,
    alignItems: 'center',
    margin: 10,
  },
  fabAddButton: {
    position: 'absolute',
    bottom: 70,
    right: 20,
    backgroundColor: 'maroon',
    borderRadius: 30,
    width: 60,
    height: 60,
    justifyContent: 'center',
    alignItems: 'center',
    elevation: 0,
  },
  buttonContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    backgroundColor: 'red',
    padding: 10,
    borderRadius: 5,
    marginHorizontal: 20,
    marginTop: 10,
    shadowColor: 'black',
    shadowOffset: { width: 0, height: 2 },
    shadowOpacity: 0.3,
    shadowRadius: 2,
    elevation: 3,
  },
  
  
});

export default App;
