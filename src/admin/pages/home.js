import React, { useState, useRef, useEffect } from 'react';
import { View, Text, Image, Dimensions, StatusBar, StyleSheet, TouchableOpacity, ScrollView, FlatList } from 'react-native';
import AsyncStorage from '@react-native-async-storage/async-storage';
import { SafeAreaView } from 'react-native-safe-area-context';

const screenWidth = Dimensions.get("window").width;

const App = ({ navigation }) => {
  const [activeIndex, setActiveIndex] = useState(0);
  const flatListRef = useRef();
  const [userId, setUserId] = useState(null);

  const getUserIdFromStorage = async () => {
    try {
      const storedUserId = await AsyncStorage.getItem("userId");
      if (storedUserId) {
        setUserId(storedUserId); // Set userId ke state jika berhasil diambil
        console.log("UserID:", storedUserId); // Panggil console.log disini
      }
    } catch (error) {
      console.error("Error retrieving userId:", error);
    }
  };
  
  // Panggil fungsi untuk mengambil userId saat komponen dimount
  useEffect(() => {
    getUserIdFromStorage();
  }, []);

  const profilePress = () => {
    navigation.navigate('PTelkomsat');
  }
  const sirihPress = () => {
    navigation.navigate('Sirih');
  }
  const daftarPress = () => {
    navigation.navigate('Daftar');
  }
  

  // Auto Scroll
  useEffect(() => {
    const autoScroll = () => {
      const nextIndex = (activeIndex + 1) % carouselData.length;
      flatListRef.current.scrollToIndex({
        animated: true,
        index: nextIndex,
      });
      setActiveIndex(nextIndex);
    };

    const interval = setInterval(autoScroll, 4000);

    return () => clearInterval(interval);
  }, [activeIndex, carouselData]);

  // Ambil Gambar
  const getItemLayout = (data, index) => ({
    length: screenWidth,
    offset: screenWidth * index,
    index: index,
  });

  // Daftar Gambar
  const carouselData = [
    {
      id: "1",
      image: require("../../asset/imageHome/home.jpg"),
    },
    {
      id: "2",
      image: require("../../asset/imageHome/image1.jpg"),
    },
    {
      id: "3",
      image: require("../../asset/imageHome/image2.jpg"),
    }
  ];

  const renderItem = ({ item, index }) => {
    return (
      <View style={styles.imageContainer}>
        <Image source={item.image} style={styles.image} />
      </View>
    )
  };

  // Mengatur Posisi Scroll
  const handleScroll = (event) => {
    const scrollPosition = event.nativeEvent.contentOffset.x

    const index = Math.round(scrollPosition / screenWidth);
    setActiveIndex(index);
  };

  // Warna Indikator Slidshow
  const renderIndicator = () => {
    return carouselData.map((dot, index) => {
      if (activeIndex === index) {
        return (
          <View
            key={index}
            style={{
              backgroundColor: 'darkred',
              height: 10, width: 10,
              borderRadius: 5,
              marginHorizontal: 5
            }}>
          </View>
        );
      } else {
        return (
          <View
            key={index}
            style={{
              backgroundColor: 'grey',
              height: 10, width: 10,
              borderRadius: 5,
              marginHorizontal: 5
            }}>
          </View>
        );
      }
    });

  };

  return (
    <SafeAreaView style={{ flex: 1, backgroundColor: '#f8f8ff' }}>
      <StatusBar barStyle={'dark-content'} backgroundColor={'#f8f8ff'} />
      <View style={{ flex: 1 }}>
        <View style={styles.header}>
          <Image source={require('../../asset/images/logoTelkomsat.png')} style={styles.menuIcon} />
        </View>
        <ScrollView>
        <View style={{ alignItems: 'center', marginTop: 15, }}>
          <FlatList
            data={carouselData}
            ref={flatListRef}
            getItemLayout={getItemLayout}
            renderItem={renderItem}
            keyExtractor={(item) => item.id}
            horizontal={true}
            pagingEnabled={true}
            onScroll={handleScroll}
          />
          <View style={{ flexDirection: 'row', justifyContent: 'center', marginTop: 8, }}>
            {renderIndicator()}
          </View>
        </View>
        
          <View style={styles.gridContainer}>
            <TouchableOpacity
              style={styles.itemContainer}
              onPress={profilePress}
            >
              <Image
                source={require('../../asset/images/factory.png')}
                style={styles.itemImage}
              />
              <Text style={styles.itemText}>Profile Telkomsat</Text>
            </TouchableOpacity>
            <TouchableOpacity
              style={styles.itemContainer}
              onPress={sirihPress}
            >
              <Image
                source={require('../../asset/images/Book.png')}
                style={styles.itemImage}
              />
              <Text style={styles.itemText}>Sekapur Sirih</Text>
            </TouchableOpacity>
            <TouchableOpacity
              style={styles.itemContainer}
              onPress={daftarPress}
            >
              <Image
                source={require('../../asset/images/books.png')}
                style={styles.itemImage}
              />
              <Text style={styles.itemText}>Daftar Buku</Text>
            </TouchableOpacity>
            <TouchableOpacity
              style={styles.itemContainer}
              onPress={() => navigation.navigate('TerakhirBaca')}
            >
              <Image
                source={require('../../asset/images/history-book.png')}
                style={styles.itemImage}
              />
              <Text style={styles.itemText}>Terakhir Baca</Text>
            </TouchableOpacity>
            <TouchableOpacity
              style={styles.itemContainer}
            //onPress={() => navigation.navigate('TerakhirBacaScreen')}
            >
              <Image
                source={require('../../asset/images/tambahBuku.png')}
                style={styles.itemImage}
              />
              <Text style={styles.itemText}>Tambah Buku</Text>
            </TouchableOpacity>
          </View>
        </ScrollView>
      </View>
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  header: {
    marginHorizontal: 20,
    flexDirection: 'row',
    marginTop: 10,
  },
  menuIcon: {
    width: 135,
    height: 50
  },
  headerText: {
    fontSize: 25,
    color: 'black',
    paddingLeft: 20,
    fontWeight: '500',
  },
  gridContainer: {
    flexDirection: 'row',
    flexWrap: 'wrap',
    justifyContent: 'space-between',
    marginHorizontal: 30,
    marginTop: 45,
  },
  imageContainer: {
    height: 200,
    width: screenWidth - 2 * 20,
    marginHorizontal: 20,
  },
  image: {
    height: '100%',
    width: '100%',
    resizeMode: 'cover',
    borderRadius: 10,
  },
  itemContainer: {
    backgroundColor: '#ffffff',
    width: '48%', // Menggunakan 48% dari lebar parent untuk jarak antar elemen
    marginBottom: 15,
    borderRadius: 20,
    alignItems: 'center',
    justifyContent: 'center',
    paddingVertical: 22,
  },
  itemText: {
    marginTop: 5,
    fontSize: 12,
    fontWeight: 'bold',
  },
  itemImage: {
    width: 50,
    height: 80,
    resizeMode: 'contain',
  },
});

export default App;
