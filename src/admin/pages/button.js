import React from 'react';
import {View, Text, TextInput, Button, TouchableOpacity, StatusBar, Image, Dimensions, ScrollView, FlatList, StyleSheet} from 'react-native';

export default function FlatButton({ text, onPress }) {
    return(
        <TouchableOpacity onPress={onPress}>
            <View style={styles.button}>
            <Text style={styles.buttonText}>{text}</Text>
            </View>
        </TouchableOpacity>

    )
}

const styles = StyleSheet.create({
    button: {
        borderRadius: 5,
        paddingVertical: 15,
        paddingHorizontal: 10,
        margin: 10,
        marginLeft: 20,
        marginRight: 20,
        backgroundColor: 'maroon',
    },
    buttonText: {
        color: 'white',
        fontWeight: 'bold',
        textTransform: 'uppercase',
        fontSize: 15,
        textAlign: 'center',
    },
        
});