  import React, { useState, useEffect } from 'react';
  import {
    StyleSheet,
    Dimensions,
    View,
    StatusBar,
    TouchableOpacity,
    ActivityIndicator,
    Alert,
    Linking,
    Text,
  } from 'react-native';
  import Pdf from 'react-native-pdf';
  import MenuBar from '../../template/MenuBar';
  import { useNavigation, useRoute } from '@react-navigation/native'; // Menghapus useState dari sini
  import Icon from 'react-native-vector-icons/Ionicons';
  import Icon2 from 'react-native-vector-icons/Fontisto';
  import AsyncStorage from '@react-native-async-storage/async-storage'; // Import AsyncStorage
  import RNFetchBlob from 'rn-fetch-blob';

  const App = () => {
    const navigation = useNavigation();
    const route = useRoute();
    const [favoriteTitles, setFavoriteTitles] = useState([]); // Tetap gunakan useState dari 'react'
    const [currentPage, setCurrentPage] = useState(1); // Simpan halaman saat ini
    const [lastViewedPage, setLastViewedPage] = useState(1);
    const [userId, setUserId] = useState(null);

  const getUserIdFromStorage = async () => {
    try {
      const storedUserId = await AsyncStorage.getItem("userId");
      if (storedUserId) {
        setUserId(storedUserId); // Set userId ke state jika berhasil diambil
        console.log("UserID:", storedUserId); // Panggil console.log disini
      }
    } catch (error) {
      console.error("Error retrieving userId:", error);
    }
  };
  
  // Panggil fungsi untuk mengambil userId saat komponen dimount
  useEffect(() => {
    getUserIdFromStorage();
  }, []);

    const onPageChanged = (page, numberOfPages) => {
      // Ketika halaman berubah, simpan halaman terakhir
      setCurrentPage(page);
    }

    const handleBuku = () => {
      navigation.goBack();
    };

    const { name, id : _id , selectedTitle, subjudul, clickedEntry, clickedTitle } = route.params;
    console.log(route.params);


    const handleSearch = () => {
      navigation.navigate('Search'); // Navigasi ke layar pencarian
    };

    const source = { uri: `http://192.168.100.7:3001/api/pdfSubjudul/${_id}/${name}`, cache: true };
    // const source = { uri: `http://192.168.100.7:3001/api/pdf/${id}/${name}`, cache: true };
    // const source = { uri: `http://192.168.100.3:3001/api/pdf/6543ada1468ad59e03a13708/topologi.pdf`, cache: true };

    const handleDownload = async () => {
      const downloadURL = source.uri;
      const destinationPath = `${RNFetchBlob.fs.dirs.DownloadDir}/${name}.pdf`;
      const downloadedBook = {
        id: Math.random().toString(36).substring(7), // Tambahkan properti id unik
        _id,
        text: clickedTitle,
        fileName: name,
        url: destinationPath,
      };
    
      try {
        const storedBooks = await AsyncStorage.getItem(`downloadedBooks_${userId}`);
        const existingBooks = storedBooks ? JSON.parse(storedBooks) : [];
    
        const existingBookIndex = existingBooks.findIndex((book) => book.text === clickedTitle);
    
        if (existingBookIndex !== -1) {
          const existingBook = existingBooks[existingBookIndex];
          if (existingBook.fileName !== name) {
            existingBooks[existingBookIndex] = downloadedBook;
            await AsyncStorage.setItem(`downloadedBooks_${userId}`, JSON.stringify(existingBooks));
            Alert.alert('Replace Successful', `${existingBook.text} replaced in downloaded books!`);
          } else {
            Alert.alert('Book Already Exists', `A book with the title "${existingBook.text}" already exists.`);
          }
        } else {
          existingBooks.push(downloadedBook);
          await AsyncStorage.setItem(`downloadedBooks_${userId}`, JSON.stringify(existingBooks));
    
          RNFetchBlob.config({
            addAndroidDownloads: {
              useDownloadManager: true,
              notification: true,
              path: destinationPath,
              description: 'PDF File',
            },
            fileCache: true,
            appendExt: 'pdf',
          })
          .fetch('GET', downloadURL)
          .then(() => {
            Alert.alert('Download Successful', `File downloaded successfully to ${destinationPath}`);
          })
          .catch((error) => {
            Alert.alert('Download Failed', `File download failed: ${error}`);
          });
          Alert.alert('Berhasil Download');
        }
      } catch (error) {
        console.error('Error handling download:', error);
      }
    };
    
    
    
    

    // Fungsi untuk menambah judul ke daftar favorit dan menyimpannya di AsyncStorage
    const addToFavorites = async (clickedTitle) => {
      try {
        if (!favoriteTitles.includes(clickedTitle)) {
          setFavoriteTitles(prevTitles => [...prevTitles, clickedTitle]);
          await AsyncStorage.setItem(`favoriteTitles_${userId}`, JSON.stringify([...favoriteTitles, clickedTitle]));
          Alert.alert('Success', `${clickedTitle} added to favorites!`);
        } else {
          Alert.alert('Already in Favorites', `${clickedTitle} is already in favorites!`);
        }
      } catch (error) {
        Alert.alert('Error', 'Failed to add to favorites');
      }
    };
    

    // favorit
    useEffect(() => {
      // Ambil daftar judul favorit dari AsyncStorage saat komponen dimuat
      const fetchData = async () => {
        try {
          const storedTitles = await AsyncStorage.getItem(`favoriteTitles_${userId}`);
          if (storedTitles) {
            setFavoriteTitles(JSON.parse(storedTitles));
          }
        } catch (error) {
          // Handle error if needed
          console.error(error);
        }
      };

      fetchData();
    }, [userId]);

    // last page
    useEffect(() => {
    console.log(`ID yang masuk: ${_id}`);
    return () => {
      // Simpan halaman terakhir saat pengguna meninggalkan halaman
      AsyncStorage.setItem(`lastPage_${_id}_${userId}`, currentPage.toString())
        .then(() => {
          console.log(`Last page ${currentPage} saved for ${subjudul}`);
        })
        .catch((error) => {
          console.error('Error saving last page:', error);
        });
    };
    }, [currentPage, _id, subjudul, userId]);

    // page terakhir dibaca
    useEffect(() => {
      const fetchData = async () => {
        try {
          const storedPage = await AsyncStorage.getItem(`lastPage_${_id}_${userId}`);
          if (storedPage) {
            setCurrentPage(parseInt(storedPage));
          }
        } catch (error) {
          console.error('Error fetching last page:', error);
        }
      };
    
      fetchData();
    }, [userId]);
    
    // page terakhir dibaca
    useEffect(() => {
      const fetchData = async () => {
        try {
          const storedPage = await AsyncStorage.getItem(`lastPage_${_id}_${userId}`);
          if (storedPage) {
            setLastViewedPage(parseInt(storedPage));
          }
        } catch (error) {
          console.error('Error fetching last page:', error);
        }
      };

      fetchData();
    }, [userId]);


    return (
      <View style={styles.container}>      
          <View style={styles.header}>
            <StatusBar barStyle={'dark-content'} backgroundColor={'#f8f8ff'} />
            <TouchableOpacity style={{ marginTop: 5 }} onPress={handleBuku}>
              <Icon name="arrow-back" size={25} color="white" />
            </TouchableOpacity>
            <View style={styles.headerTextContainer}>
              <View style={styles.titleContainer}>
                <Text style={styles.headerText} numberOfLines={1} ellipsizeMode="tail">{subjudul}</Text>
              </View>
              <View style={styles.buttonContainer}>
                <TouchableOpacity style={styles.favoriteButton} onPress={() => addToFavorites(clickedTitle)}>
                  <Icon2 name="favorite" size={25} color="white" />
                </TouchableOpacity>
                <TouchableOpacity style={styles.downloadButton} onPress={() => handleDownload(clickedTitle)}>
                  <Icon name="download" size={25} color="white" />
                </TouchableOpacity>
                <TouchableOpacity onPress={handleSearch}>
                  <Icon name="search" size={25} color="white" />
                </TouchableOpacity>
              </View>
            </View>
          </View>
        <View>
          <Pdf
            trustAllCerts={false}
            source={source}
            page={lastViewedPage}
            password='123'
            onLoadComplete={(numberOfPages, filePath) => {
              console.log(`Number of pages: ${numberOfPages}`);
            }}
            onPageChanged={(page, numberOfPages) => onPageChanged(page, numberOfPages)}
            onError={error => {
              if (typeof error === 'string') {
                if (error.includes("Use of own trust manager but none defined")) {
                  // Handle SSL error
                  console.error("SSL error: " + error);
                } else {
                  // Handle other types of errors
                  console.error(error);
                }
              } else {
                // Handle non-string errors or do other appropriate actions
                console.error("Non-string error:", error);
              }
            }}
            
            onPressLink={uri => {
              console.log(`Link pressed: ${uri}`);
              // Handle link press, e.g., use Linking.openURL(uri)
            }}
            style={styles.pdf}
          />
        </View>
        <MenuBar />
      </View>
    );
  }

  const styles = StyleSheet.create({
    container: {
      flex: 1,
      justifyContent: 'flex-start',
      alignItems: 'center',
    },
    pdf: {
      flex: 1,
      width: Dimensions.get('window').width,
      height: Dimensions.get('window').height,
    },
    header: {
      flexDirection: 'row',
      alignItems: 'center',
      padding: 15,
      backgroundColor: 'red',
      borderBottomWidth: 1,
      borderBottomColor: 'red',
    },  
    headerText: {
      fontSize: 25,
      color: 'white',
      fontWeight: 'bold',
      maxWidth: '90%', // Atur maksimum lebar teks untuk header
    },
    searchIcon: {
      marginHorizontal: 10,
    },
    headerTextContainer: {
      flexDirection: 'row',
      alignItems: 'center',
      paddingHorizontal: 10,
      flex: 1,
      justifyContent: 'space-between',
    },
    titleContainer: {
      flex: 1,
    },
    buttonContainer: {
      flexDirection: 'row',
      alignItems: 'center',
    },
    downloadSearchContainer: {
      flexDirection: 'row',
      alignItems: 'center',
    },
    downloadButton: {
      marginHorizontal: 10, // Sesuaikan margin agar lebih dekat
    },
    favoriteButton: {
      marginHorizontal: 10, // Sesuaikan margin agar lebih dekat
    },
  });

  export default App;