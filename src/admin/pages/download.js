import React, { useState, useEffect } from 'react';
import { View, Text, TouchableOpacity, FlatList, StyleSheet, Alert } from 'react-native';
import AsyncStorage from '@react-native-async-storage/async-storage';
import Icon from 'react-native-vector-icons/Ionicons';
import RNFS from 'react-native-fs';
import MenuBar from '../../template/MenuBar';
import { check, PERMISSIONS, request } from 'react-native-permissions';


const DownloadPage = ({ navigation }) => {
  const [downloadedBooks, setDownloadedBooks] = useState([]);
  const [userId, setUserId] = useState(null);

  const getUserIdFromStorage = async () => {
    try {
      const storedUserId = await AsyncStorage.getItem("userId");
      if (storedUserId) {
        setUserId(storedUserId); // Set userId ke state jika berhasil diambil
        console.log("UserID:", storedUserId); // Panggil console.log disini
      }
    } catch (error) {
      console.error("Error retrieving userId:", error);
    }
  };
  
  // Panggil fungsi untuk mengambil userId saat komponen dimount
  useEffect(() => {
    getUserIdFromStorage();
  }, []);

  useEffect(() => {
    const fetchDownloadedBooks = async () => {
      try {
        const storedBooks = await AsyncStorage.getItem(`downloadedBooks_${userId}`);
        if (storedBooks) {
          setDownloadedBooks(JSON.parse(storedBooks));
        } else {
          setDownloadedBooks([]); // Pastikan state terinisialisasi dengan array kosong jika tidak ada data
        }
      } catch (error) {
        console.error('Error fetching downloaded books:', error);
      }
    };

    fetchDownloadedBooks();
  }, [userId]);

  // const fetchDownloadedBooks = async () => {
  //   try {
  //     const storedBooks = await AsyncStorage.getItem(`downloadedBooks_${userId}`);
  //     if (storedBooks) {
  //       const parsedBooks = JSON.parse(storedBooks);
  //       const validBooks = parsedBooks.filter(item => item && item.id !== null); // Filtering out null or invalid items
  //       setDownloadedBooks(validBooks);
  //     } else {
  //       setDownloadedBooks([]);
  //     }
  //   } catch (error) {
  //     console.error('Error fetching downloaded books:', error);
  //   }
  // };
  

  const openBook = async (book) => {
    const expectedPath = RNFS.ExternalStorageDirectoryPath + '/Download/' + book.fileName + '.pdf';
    console.log('Expected Path:', expectedPath); // Log expected path for debugging
    
    try {
      const exists = await RNFS.exists(expectedPath); // Check file existence at the correct path
      console.log('File exists:', exists); // Log file existence for debugging
  
      if (exists) {
        navigation.navigate('Offline', { book, path: expectedPath, title: book.text});
      } else {
        console.log('File not found at expected path:', expectedPath);
      }
    } catch (error) {
      console.error('Error checking file existence:', error);
    }
  };
  
  

  const handleDelete = async (itemId) => {
    // Fungsi untuk menampilkan konfirmasi sebelum menghapus buku
    const confirmDelete = () => {
      Alert.alert(
        'Delete Book',
        'Are you sure you want to delete this book?',
        [
          {
            text: 'Cancel',
            style: 'cancel',
          },
          {
            text: 'Delete',
            style: 'destructive',
            onPress: async () => {
              // Hapus item dari state dan AsyncStorage dengan membandingkan ID
              setDownloadedBooks((prevData) => prevData.filter((item) => item.id !== itemId));
            
              try {
                const storedBooks = await AsyncStorage.getItem(`downloadedBooks_${userId}`);
                if (storedBooks) {
                  const updatedBooks = JSON.parse(storedBooks).filter((item) => item.id !== itemId);
                  AsyncStorage.setItem(`downloadedBooks_${userId}`, JSON.stringify(updatedBooks));
                }
              } catch (error) {
                console.error('Error deleting downloaded book:', error);
              }
            },
          },
        ],
        { cancelable: false }
      );
    };
  
    // Tampilkan konfirmasi
    confirmDelete();
  };
  
  

  // const renderBookItem = ({ item }) => (
  //   <TouchableOpacity onPress={() => openBook(item)} style={styles.bookItem}>
  //     <Text style={styles.bookTitle}>{item.text}</Text>
  //     <Icon name="trash-bin-outline" size={20} color="red" onPress={() => handleDelete(item.id)} />
  //   </TouchableOpacity>
  // );

  const renderBookItem = ({ item }) => (
    <TouchableOpacity onPress={() => openBook(item)}>
      <View style={styles.bookItemContainer}>
        <View style={styles.bookDetails}>
          <Text style={styles.bookTitle}>{item.text}</Text>
        </View>
        <TouchableOpacity onPress={() => handleDelete(item.id)} style={{padding: 10}}>
          <Icon name="trash-outline" size={20} color="red" />
        </TouchableOpacity>
      </View>
      <View style={styles.separator} />
    </TouchableOpacity>
  );

  // Fungsi untuk memeriksa dan meminta izin
  const requestStoragePermission = async () => {
    try {
      const storagePermission = await check(PERMISSIONS.ANDROID.READ_EXTERNAL_STORAGE);
      if (storagePermission === 'granted') {
        // Izin sudah diberikan
        console.log('Storage permission granted');
      } else if (storagePermission === 'denied') {
        // Izin belum diberikan, minta izin
        const newPermission = await request(PERMISSIONS.ANDROID.READ_EXTERNAL_STORAGE);
        if (newPermission === 'granted') {
          console.log('Storage permission granted');
        }
      }
    } catch (error) {
      console.error('Error checking storage permission:', error);
    }
  };

  // Panggil fungsi untuk meminta izin saat aplikasi berjalan
  requestStoragePermission();

  

  return (
    <View style={styles.container}>
      <View style={styles.header}>
        <TouchableOpacity style={styles.backButton} onPress={() => navigation.navigate('Home')}>
          <Icon name="arrow-back" size={25} color="white" />
        </TouchableOpacity>
        <Text style={styles.headerText}>Downloaded</Text>
      </View>

      {/* <FlatList
        data={downloadedBooks}
        keyExtractor={(item, index) => (item.id ? item.id.toString() : index.toString())}
        renderItem={renderBookItem}
        ListEmptyComponent={
          <View style={styles.noBooksContainer}>
            <Text style={styles.noBooksText}>No downloaded books</Text>
          </View>
        }
      /> */}

      {downloadedBooks.length > 0 ? (
        <FlatList
          data={downloadedBooks}
          keyExtractor={(item, index) => (item.id ? item.id.toString() : index.toString())}
          renderItem={renderBookItem}
        />
      ) : (
        <View style={styles.noBooksContainer}>
          <Text style={styles.noBooksText}>No downloaded books yet</Text>
        </View>
      )}



      <MenuBar />
    </View>
    

  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white',
  },
  header: {
    flexDirection: 'row',
    alignItems: 'center',
    paddingVertical: 15,
    paddingHorizontal: 20,
    backgroundColor: 'maroon',
    borderBottomWidth: 1,
    borderBottomColor: 'maroon',
  },
  headerText: {
    fontSize: 25,
    color: 'white',
    fontWeight: 'bold',
    marginLeft: 20,
  },
  backButton: {
    padding: 10,
  },
  bookItem: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    padding: 16,
    borderBottomWidth: 1,
    borderBottomColor: '#ccc',
  },
  bookTitle: {
    fontSize: 18,
    fontWeight: 'bold',
    marginRight: 16,
  },
  noBooksContainer: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  noBooksText: {
    textAlign: 'center',
    marginTop: 20,
    fontSize: 18,
    color: 'gray',
  },
  bookItemContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    padding: 16,
  },
  bookDetails: {
    flex: 1,
  },
  separator: {
    height: 1,
    backgroundColor: '#ccc',
    // marginHorizontal: 20,
  },
});

export default DownloadPage;